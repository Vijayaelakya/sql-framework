MERGE
  mob.mob_email_template_audit T
USING
  (SELECT DISTINCT 
   audit_id
   id,
   template_id,
    template_name as template_name
FROM staging.mob_mobilityprod_email_template_audit) S
ON
T.audit_id = S.audit_id
WHEN MATCHED THEN UPDATE SET
T.audit_id = S.audit_id,
T.id = S.id,
T.template_id = S.template_id,
T.template_name = S.template_name
  WHEN NOT MATCHED
  THEN
INSERT
(audit_id, id, template_id, template_name)
VALUES (S.audit_id, S.id, S.template_id, S.template_name)
