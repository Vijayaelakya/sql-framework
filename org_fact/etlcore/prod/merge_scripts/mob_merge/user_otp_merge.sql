MERGE
  mob.mob_user_otp T
USING
  (SELECT DISTINCT
   user_otp_id,
   blocked_till_date_time,
   last_otp_generation_date_time,
   unsuccessful_attempt_count,
    mobile as mobile,
   otp_verified,
   otp_generated
FROM staging.mob_mobilityprod_user_otp) S
ON
T.user_otp_id = S.user_otp_id
WHEN MATCHED THEN UPDATE SET
T.user_otp_id = S.user_otp_id,
T.blocked_till_date_time = datetime(S.blocked_till_date_time),
T.last_otp_generation_date_time = datetime(S.last_otp_generation_date_time),
T.unsuccessful_attempt_count = S.unsuccessful_attempt_count,
T.otp_generated = S.otp_generated,
T.mobile = S.mobile,
T.otp_verified = S.otp_verified
  WHEN NOT MATCHED
  THEN
INSERT
(user_otp_id,blocked_till_date_time,last_otp_generation_date_time,unsuccessful_attempt_count,mobile,otp_verified,otp_generated)
VALUES (S.user_otp_id,datetime(S.blocked_till_date_time),datetime(S.last_otp_generation_date_time),S.unsuccessful_attempt_count,S.mobile,S.otp_verified,S.otp_generated)

