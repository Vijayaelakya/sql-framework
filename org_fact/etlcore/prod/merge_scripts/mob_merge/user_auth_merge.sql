MERGE
  mob.mob_user_auth T
USING
  (SELECT DISTINCT 
   user_auth_id,
    client as client,
    guid as guid,
    refresh_token as refresh_token,
   refresh_token_expiration_time,
   refresh_token_generation_time,
    user_status as user_status,
   user_id
FROM staging.mob_mobilityprod_user_auth) S
ON
T.user_auth_id = S.user_auth_id
WHEN MATCHED THEN UPDATE SET
T.user_auth_id = S.user_auth_id,
T.client = S.client,
T.guid = S.guid,
T.refresh_token = S.refresh_token,
T.refresh_token_expiration_time = datetime(S.refresh_token_expiration_time),
T.refresh_token_generation_time = datetime(S.refresh_token_generation_time),
T.user_status = S.user_status,
T.user_id = S.user_id
  WHEN NOT MATCHED
  THEN
INSERT
(user_auth_id,client,guid,refresh_token,refresh_token_expiration_time,refresh_token_generation_time,user_status,user_id)
VALUES (S.user_auth_id,S.client,S.guid,S.refresh_token,datetime(S.refresh_token_expiration_time),datetime(S.refresh_token_generation_time),S.user_status,S.user_id)
