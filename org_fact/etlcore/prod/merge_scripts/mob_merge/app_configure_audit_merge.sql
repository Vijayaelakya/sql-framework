MERGE
  mob.mob_app_configure_audit T
USING
  (SELECT DISTINCT 
   audit_id,
   version_id,
    os as os,
   upgrade_type,
   version_code
FROM staging.mob_mobilityprod_app_configure_audit) S
ON
T.audit_id = S.audit_id
WHEN MATCHED THEN UPDATE SET
T.audit_id = S.audit_id,
T.version_id = S.version_id,
T.os = S.os,
T.upgrade_type = S.upgrade_type,
T.version_code = S.version_code
  WHEN NOT MATCHED
  THEN
INSERT
(audit_id,version_id,os,upgrade_type,version_code)
VALUES (S.audit_id,S.version_id,S.os,S.upgrade_type,S.version_code)
