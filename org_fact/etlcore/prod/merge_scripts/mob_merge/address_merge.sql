MERGE mob.mob_address T
USING (
  SELECT DISTINCT address_id
    ,building_code
    ,building_desc
    ,building_floor
    ,building_name
    ,building_no
    ,building_unit
    ,postal_code
    ,street_name
    ,user_id
  FROM staging.mob_mobilityprod_address
  ) S
  ON T.address_id = S.address_id
WHEN MATCHED
  THEN
    UPDATE
    SET T.address_id = S.address_id
      ,T.building_code = S.building_code
      ,T.building_desc = S.building_desc
      ,T.building_floor = S.building_floor
      ,T.building_name = S.building_name
      ,T.building_no = S.building_no
      ,T.building_unit = S.building_unit
      ,T.postal_code = S.postal_code
      ,T.street_name = S.street_name
      ,T.user_id = S.user_id
WHEN NOT MATCHED
  THEN
    INSERT (
      address_id
      ,building_code
      ,building_desc
      ,building_floor
      ,building_name
      ,building_no
      ,building_unit
      ,postal_code
      ,street_name
      ,user_id
      )
    VALUES (
      S.address_id
      ,S.building_code
      ,S.building_desc
      ,S.building_floor
      ,S.building_name
      ,S.building_no
      ,S.building_unit
      ,S.postal_code
      ,S.street_name
      ,S.user_id
      );
