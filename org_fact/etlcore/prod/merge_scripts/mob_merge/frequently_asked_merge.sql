MERGE
  mob.mob_frequently_asked T
USING
  (SELECT DISTINCT  
   faq_id,
    answers as answers,
    question as question
FROM staging.mob_mobilityprod_frequently_asked) S
ON
T.faq_id = S.faq_id
WHEN MATCHED THEN UPDATE SET
T.faq_id = S.faq_id,
T.answers = S.answers,
T.question = S.question
  WHEN NOT MATCHED
  THEN
INSERT
(faq_id, answers, question)
VALUES (S.faq_id, S.answers, S.question)
