MERGE
  mob.mob_email_template T
USING
  (SELECT DISTINCT 
   id,
   template_id,
    template_name as template_name
FROM staging.mob_mobilityprod_email_template) S
ON
T.id = S.id
WHEN MATCHED THEN UPDATE SET
T.id = S.id,
T.template_id = S.template_id,
T.template_name = S.template_name
  WHEN NOT MATCHED
  THEN
INSERT
(id,template_id,template_name)
VALUES (S.id,S.template_id,S.template_name)
