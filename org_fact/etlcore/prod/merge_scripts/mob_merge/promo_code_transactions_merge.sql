MERGE
  mob.mob_promo_code_transactions T
USING
  (SELECT DISTINCT 
       user_id
       ,promo_id
       ,promo_tran_id
       ,is_award_success
       ,failure_message AS failure_message
       ,transaction_log_time
       ,last_updated
FROM staging.mob_mobilityprod_promo_code_transactions) S
ON
T.promo_tran_id = S.promo_tran_id
WHEN MATCHED THEN UPDATE SET
T.promo_tran_id = S.promo_tran_id,
T.user_id = S.user_id,
T.promo_id = S.promo_id,
T.is_award_success = S.is_award_success,
T.failure_message = S.failure_message,
T.transaction_log_time = datetime(S.transaction_log_time),
T.last_updated = datetime(S.last_updated)
  WHEN NOT MATCHED
  THEN
INSERT
(promo_tran_id,user_id,promo_id,is_award_success,failure_message,transaction_log_time,last_updated)
VALUES (S.promo_tran_id,S.user_id,S.promo_id,S.is_award_success,S.failure_message,datetime(S.transaction_log_time),datetime(S.last_updated))
