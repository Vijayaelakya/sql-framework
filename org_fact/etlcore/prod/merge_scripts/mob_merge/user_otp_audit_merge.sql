MERGE
  mob.mob_user_otp_audit T
USING
  (SELECT DISTINCT
   audit_id,
   user_otp_id,
    mobile as mobile,
   blocked_till_date_time,
   last_otp_generation_date_time,
   unsuccessful_attempt_count,
   otp_verified
FROM staging.mob_mobilityprod_user_otp_audit) S
ON
T.audit_id = S.audit_id
WHEN MATCHED THEN UPDATE SET
T.audit_id = S.audit_id,
T.user_otp_id = S.user_otp_id,
T.mobile = S.mobile,
T.blocked_till_date_time = datetime(S.blocked_till_date_time),
T.last_otp_generation_date_time = datetime(S.last_otp_generation_date_time),
T.unsuccessful_attempt_count = S.unsuccessful_attempt_count,
T.otp_verified = S.otp_verified
  WHEN NOT MATCHED
  THEN
INSERT
(audit_id,user_otp_id,mobile,blocked_till_date_time,last_otp_generation_date_time,unsuccessful_attempt_count,otp_verified)
VALUES (S.audit_id,S.user_otp_id,S.mobile,datetime(S.blocked_till_date_time),datetime(S.last_otp_generation_date_time),S.unsuccessful_attempt_count,S.otp_verified)
