MERGE mob.mob_user_transaction_log T
USING (
  SELECT DISTINCT user_transaction_log_id
    ,user_transaction_id
    ,aid
    ,from_state
    ,to_state
    ,is_success
    ,failure_reason
    ,trigger_source
    ,merchant_id
    ,qr_code
    ,net_amount
    ,redeem_deal
    ,deal_id
    ,deal_discount
    ,claim_status
    ,deal_record_id
    ,store_id
    ,is_cls_merchant
    ,txn_data
    ,redeem_lp
    ,burn_status
    ,burn_lp
    ,cls_burn_ref_no
    ,earn_status
    ,earned_lp
    ,cls_earn_ref_no
    ,geox
    ,geoy
    ,ip_addr
    ,transaction_log_time
  FROM staging.mob_mobilityprod_user_transaction_log
  ) S
  ON T.user_transaction_log_id = S.user_transaction_log_id
WHEN MATCHED
  THEN
    UPDATE
    SET T.user_transaction_log_id = S.user_transaction_log_id
      ,T.user_transaction_id = S.user_transaction_id
      ,T.aid = S.aid
      ,T.from_state = S.from_state
      ,T.to_state = S.to_state
      ,T.is_success = S.is_success
      ,T.failure_reason = S.failure_reason
      ,T.trigger_source = S.trigger_source
      ,T.merchant_id = S.merchant_id
      ,T.qr_code = S.qr_code
      ,T.net_amount = CAST(S.net_amount AS NUMERIC)
      ,T.redeem_deal = S.redeem_deal
      ,T.deal_id = S.deal_id
      ,T.deal_discount = CAST(S.deal_discount AS NUMERIC)
      ,T.claim_status = S.claim_status
      ,T.deal_record_id = S.deal_record_id
      ,T.store_id = S.store_id
      ,T.is_cls_merchant = S.is_cls_merchant
      ,T.txn_data = S.txn_data
      ,T.redeem_lp = S.redeem_lp
      ,T.burn_status = S.burn_status
      ,T.burn_lp = CAST(S.burn_lp AS NUMERIC)
      ,T.cls_burn_ref_no = S.cls_burn_ref_no
      ,T.earn_status = S.earn_status
      ,T.earned_lp = S.earned_lp
      ,T.cls_earn_ref_no = S.cls_earn_ref_no
      ,T.geox = S.geox
      ,T.geoy = S.geoy
      ,T.ip_addr = S.ip_addr
      ,T.transaction_log_time = datetime(S.transaction_log_time)
WHEN NOT MATCHED
  THEN
    INSERT (
      user_transaction_log_id
      ,user_transaction_id
      ,aid
      ,from_state
      ,to_state
      ,is_success
      ,failure_reason
      ,trigger_source
      ,merchant_id
      ,qr_code
      ,net_amount
      ,redeem_deal
      ,deal_id
      ,deal_discount
      ,claim_status
      ,deal_record_id
      ,store_id
      ,is_cls_merchant
      ,txn_data
      ,redeem_lp
      ,burn_status
      ,burn_lp
      ,cls_burn_ref_no
      ,earn_status
      ,earned_lp
      ,cls_earn_ref_no
      ,geox
      ,geoy
      ,ip_addr
      ,transaction_log_time
      )
    VALUES (
      S.user_transaction_log_id
      ,S.user_transaction_id
      ,S.aid
      ,S.from_state
      ,S.to_state
      ,S.is_success
      ,S.failure_reason
      ,S.trigger_source
      ,S.merchant_id
      ,S.qr_code
      ,CAST(S.net_amount AS NUMERIC)
      ,S.redeem_deal
      ,S.deal_id
      ,CAST(S.deal_discount AS NUMERIC)
      ,S.claim_status
      ,S.deal_record_id
      ,S.store_id
      ,S.is_cls_merchant
      ,S.txn_data
      ,S.redeem_lp
      ,S.burn_status
      ,CAST(S.burn_lp AS NUMERIC)
      ,S.cls_burn_ref_no
      ,S.earn_status
      ,S.earned_lp
      ,S.cls_earn_ref_no
      ,S.geox
      ,S.geoy
      ,S.ip_addr
      ,datetime(S.transaction_log_time)
      );
