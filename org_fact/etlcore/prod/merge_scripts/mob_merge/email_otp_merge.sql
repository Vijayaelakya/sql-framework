MERGE
  mob.mob_email_otp T
USING
  (SELECT DISTINCT
   user_otp_id,
   blocked_till_date_time,
    email as email,
   last_otp_generation_date_time,
    otp as otp,
   unsuccessful_attempt_count,
   user_id
FROM staging.mob_mobilityprod_email_otp) S
ON
T.user_otp_id = S.user_otp_id
WHEN MATCHED THEN UPDATE SET
T.user_otp_id = S.user_otp_id,
T.blocked_till_date_time = datetime(S.blocked_till_date_time),
T.email = S.email,
T.last_otp_generation_date_time = datetime(S.last_otp_generation_date_time),
T.otp = S.otp,
T.unsuccessful_attempt_count = S.unsuccessful_attempt_count,
T.user_id = S.user_id
  WHEN NOT MATCHED
  THEN
INSERT
(user_otp_id,blocked_till_date_time,email,last_otp_generation_date_time,otp,unsuccessful_attempt_count,user_id)
VALUES (S.user_otp_id,datetime(S.blocked_till_date_time),S.email,datetime(S.last_otp_generation_date_time),S.otp,S.unsuccessful_attempt_count,S.user_id)
