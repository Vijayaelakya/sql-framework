MERGE
  mob.mob_user_meta T
USING
  (SELECT DISTINCT 
   user_meta_id,
   meta_info,
   user_id
FROM staging.mob_mobilityprod_user_meta) S
ON
T.user_meta_id = S.user_meta_id
WHEN MATCHED THEN UPDATE SET
T.user_meta_id = S.user_meta_id,
T.meta_info = S.meta_info,
T.user_id = S.user_id
  WHEN NOT MATCHED
  THEN
INSERT
(user_meta_id,meta_info,user_id)
VALUES (S.user_meta_id,S.meta_info,S.user_id)
