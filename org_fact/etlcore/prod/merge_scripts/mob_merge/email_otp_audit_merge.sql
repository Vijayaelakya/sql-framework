MERGE
  mob.mob_email_otp_audit T
USING
  (SELECT DISTINCT 
   audit_id,
   user_otp_id,
    email as email,
    otp as otp,
   blocked_till_date_time,
   last_otp_generation_date_time,
   unsuccessful_attempt_count,
   user_id
FROM staging.mob_mobilityprod_email_otp_audit) S
ON
T.audit_id = S.audit_id
WHEN MATCHED THEN UPDATE SET
T.audit_id = S.audit_id,
T.user_otp_id = S.user_otp_id,
T.email = S.email,
T.otp = S.otp,
T.blocked_till_date_time = datetime(S.blocked_till_date_time),
T.last_otp_generation_date_time = datetime(S.last_otp_generation_date_time),
T.unsuccessful_attempt_count = S.unsuccessful_attempt_count,
T.user_id = S.user_id
  WHEN NOT MATCHED
  THEN
INSERT
(audit_id,user_otp_id,email,otp,blocked_till_date_time,last_otp_generation_date_time,unsuccessful_attempt_count,user_id)
VALUES (S.audit_id,S.user_otp_id,S.email,S.otp,datetime(S.blocked_till_date_time),datetime(S.last_otp_generation_date_time),S.unsuccessful_attempt_count,S.user_id)
