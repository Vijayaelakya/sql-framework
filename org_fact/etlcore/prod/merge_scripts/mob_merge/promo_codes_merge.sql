MERGE
  mob.mob_promo_codes T
USING
  (SELECT DISTINCT 
promo_id,
promo_code as promo_code,
start_date,
end_date,
sub_source,
action_code,
promo_award_msg as promo_award_msg,
id,
event_name
FROM staging.mob_mobilityprod_promo_codes) S
ON
T.promo_id = S.promo_id
WHEN MATCHED THEN UPDATE SET
T.promo_id = S.promo_id,
T.promo_code = S.promo_code,
T.start_date = datetime(S.start_date),
T.end_date = datetime( S.end_date),
T.sub_source = S.sub_source,
T.action_code = S.action_code,
T.promo_award_msg = S.promo_award_msg,
T.id = S.id,
T.event_name = S.event_name
  WHEN NOT MATCHED
  THEN
INSERT
(promo_id,promo_code,start_date,end_date,sub_source,action_code,promo_award_msg,id,event_name)
VALUES (S.promo_id,S.promo_code,datetime(S.start_date),datetime(S.end_date),S.sub_source,S.action_code,S.promo_award_msg,S.id,S.event_name)
