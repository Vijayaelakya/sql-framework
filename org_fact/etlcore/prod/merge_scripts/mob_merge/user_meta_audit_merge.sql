MERGE
  mob.mob_user_meta_audit T
USING
  (SELECT DISTINCT 
     audit_id,
  user_meta_id,
  meta_info,
  user_id
FROM staging.mob_mobilityprod_user_meta_audit) S
ON
T.audit_id = S.audit_id
WHEN MATCHED THEN UPDATE SET
T.audit_id = S.audit_id,
T.user_meta_id = S.user_meta_id,
T.meta_info = S.meta_info,
T.user_id = S.user_id
  WHEN NOT MATCHED
  THEN
INSERT
(audit_id,user_meta_id,meta_info,user_id)
VALUES (S.audit_id,S.user_meta_id,S.meta_info,S.user_id)
