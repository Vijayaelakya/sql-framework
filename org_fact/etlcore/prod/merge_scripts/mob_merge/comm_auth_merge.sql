MERGE

  mob.mob_comm_auth T
USING
  (SELECT DISTINCT 
   id,
    api as api,
    api_key as api_key
FROM staging.mob_mobilityprod_comm_auth) S
ON
T.id = S.id
WHEN MATCHED THEN UPDATE SET
T.id = S.id,
T.api = S.api,
T.api_key = S.api_key
  WHEN NOT MATCHED
  THEN
INSERT
(id,api,api_key)
VALUES (S.id,S.api,S.api_key)
