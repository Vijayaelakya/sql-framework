MERGE
  mob.mob_comm_auth_audit T
USING
  (SELECT DISTINCT 
   audit_id,
   id,
    api as api,
    api_key as api_key
FROM staging.mob_mobilityprod_comm_auth_audit) S
ON
T.audit_id = S.audit_id
WHEN MATCHED THEN UPDATE SET
T.audit_id = S.audit_id,
T.id = S.id,
T.api = S.api,
T.api_key = S.api_key
  WHEN NOT MATCHED
  THEN
INSERT
(audit_id,id,api,api_key)
VALUES (S.audit_id,S.id,S.api,S.api_key)
