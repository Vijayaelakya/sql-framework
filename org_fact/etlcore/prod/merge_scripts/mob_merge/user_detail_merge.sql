MERGE
  mob.mob_user_detail T
USING
  (SELECT DISTINCT
   user_id,
    aid as aid,
    cards_series as cards_series,
   created_date,
    dob as dob,
    email as email,
   email_verified,
    fname as fname,
   id_type,
   is_blocked,
   otp_verified,
    lname as lname,
    mobile as mobile,
    nric_or_other_id as nric_or_other_id,
   opt_in_push_notification,
    source as source,
   unlock_deals,
   last_updated,
   claim_first_stamp,
   dob_deal_unlock,
   stamp_deal_unlock,
   email_deal_unlock,
   is_link_points_member,
    race as race,
    nationality as nationality,
    gender as gender,
    access_token as access_token
FROM staging.mob_mobilityprod_user_detail) S
ON
T.user_id = S.user_id
WHEN MATCHED THEN UPDATE SET
T.user_id = S.user_id,
T.aid = S.aid,
T.cards_series = S.cards_series,
T.created_date = datetime(S.created_date),
T.dob = S.dob,
T.email = S.email,
T.email_verified = S.email_verified,
T.fname = S.fname,
T.id_type = S.id_type,
T.is_blocked = S.is_blocked,
T.otp_verified = S.otp_verified,
T.lname = S.lname,
T.mobile = S.mobile,
T.nric_or_other_id = S.nric_or_other_id,
T.opt_in_push_notification = S.opt_in_push_notification,
T.source = S.source,
T.unlock_deals = S.unlock_deals,
T.last_updated = datetime(S.last_updated),
T.claim_first_stamp = S.claim_first_stamp,
T.dob_deal_unlock = S.dob_deal_unlock,
T.stamp_deal_unlock = S.stamp_deal_unlock,
T.email_deal_unlock = S.email_deal_unlock,
T.is_link_points_member = S.is_link_points_member,
T.race = S.race,
T.nationality = S.nationality,
T.gender = S.gender,
T.access_token = S.access_token
  WHEN NOT MATCHED
  THEN
INSERT
(user_id,aid,cards_series,created_date,dob,email,email_verified,fname,id_type,is_blocked,otp_verified,lname,mobile,nric_or_other_id,opt_in_push_notification,source,unlock_deals,last_updated,claim_first_stamp,dob_deal_unlock,stamp_deal_unlock,email_deal_unlock,is_link_points_member,race,nationality,gender,access_token)
VALUES (S.user_id,S.aid,S.cards_series,datetime(S.created_date),S.dob,S.email,S.email_verified,S.fname,S.id_type,S.is_blocked,S.otp_verified,S.lname,S.mobile,S.nric_or_other_id,S.opt_in_push_notification,S.source,S.unlock_deals,datetime(S.last_updated),S.claim_first_stamp,S.dob_deal_unlock,S.stamp_deal_unlock,S.email_deal_unlock,S.is_link_points_member,S.race,S.nationality,S.gender,S.access_token)
