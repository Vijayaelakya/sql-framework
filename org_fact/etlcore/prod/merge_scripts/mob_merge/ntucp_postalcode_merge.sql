MERGE
  mob.mob_ntucp_postalcode T
USING
  (SELECT DISTINCT 
   postal_code as postal_code,
   building_no as building_no,
   street_name as street_name,
   building_name as building_name,
   building_code as building_code,
   building_desc as building_desc
FROM staging.mob_mobilityprod_ntucp_postalcode) S
ON
T.postal_code = S.postal_code
WHEN MATCHED THEN UPDATE SET
T.postal_code = S.postal_code,
T.building_no = S.building_no,
T.street_name = S.street_name,
T.building_name = S.building_name,
T.building_code = S.building_code,
T.building_desc = S.building_desc
  WHEN NOT MATCHED
  THEN
INSERT
(postal_code,building_no,street_name,building_name,building_code,building_desc)
VALUES (S.postal_code,S.building_no,S.street_name,S.building_name,S.building_code,S.building_desc)
