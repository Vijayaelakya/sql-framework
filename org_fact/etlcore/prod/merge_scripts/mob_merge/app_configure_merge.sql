MERGE
  mob.mob_app_configure T
USING
  (SELECT DISTINCT 
   version_id,
    os as os,
   upgrade_type,
   version_code 
FROM staging.mob_mobilityprod_app_configure) S
ON
T.version_id = S.version_id
WHEN MATCHED THEN UPDATE SET
T.version_id = S.version_id,
T.os = S.os,
T.upgrade_type = S.upgrade_type,
T.version_code = S.version_code
  WHEN NOT MATCHED
  THEN
INSERT
(version_id,os,upgrade_type,version_code)
VALUES (S.version_id,S.os,S.upgrade_type,S.version_code)
