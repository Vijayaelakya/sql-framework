MERGE mob.mob_user_transaction_record T
USING (
  SELECT DISTINCT user_transaction_id AS user_transaction_id
    ,aid AS aid
    ,current_state
    ,trigger_source
    ,merchant_id
    ,qr_code AS qr_code
    ,purchase_amount
    ,net_amount
    ,redeem_deal
    ,deal_id
    ,deal_discount
    ,claim_status
    ,deal_record_id
    ,store_id
    ,is_cls_merchant
    ,txn_data AS txn_data
    ,redeem_lp
    ,burn_status
    ,burn_lp
    ,cls_burn_ref_no AS cls_burn_ref_no
    ,earn_status
    ,earned_lp
    ,cls_earn_ref_no AS cls_earn_ref_no
    ,transaction_start_date
    ,transaction_end_date
    ,transaction_expiry_date
    ,transaction_updated_date
  FROM staging.mob_mobilityprod_user_transaction_record
  ) S
  ON T.user_transaction_id = S.user_transaction_id
WHEN MATCHED
  THEN
    UPDATE
    SET T.user_transaction_id = S.user_transaction_id
      ,T.aid = S.aid
      ,T.current_state = S.current_state
      ,T.trigger_source = S.trigger_source
      ,T.merchant_id = S.merchant_id
      ,T.qr_code = S.qr_code
      ,T.purchase_amount = CAST(S.purchase_amount AS NUMERIC)
      ,T.net_amount = CAST(S.net_amount AS NUMERIC)
      ,T.redeem_deal = S.redeem_deal
      ,T.deal_id = S.deal_id
      ,T.deal_discount = CAST(S.deal_discount AS NUMERIC)
      ,T.claim_status = S.claim_status
      ,T.deal_record_id = S.deal_record_id
      ,T.store_id = S.store_id
      ,T.is_cls_merchant = S.is_cls_merchant
      ,T.txn_data = S.txn_data
      ,T.redeem_lp = S.redeem_lp
      ,T.burn_status = S.burn_status
      ,T.burn_lp = CAST(S.burn_lp AS NUMERIC)
      ,T.cls_burn_ref_no = S.cls_burn_ref_no
      ,T.earn_status = S.earn_status
      ,T.earned_lp = S.earned_lp
      ,T.cls_earn_ref_no = S.cls_earn_ref_no
      ,T.transaction_start_date = datetime(S.transaction_start_date)
      ,T.transaction_end_date = datetime(S.transaction_end_date)
      ,T.transaction_expiry_date = datetime(S.transaction_expiry_date)
      ,T.transaction_updated_date = datetime(S.transaction_updated_date)
WHEN NOT MATCHED
  THEN
    INSERT (
      user_transaction_id
      ,aid
      ,current_state
      ,trigger_source
      ,merchant_id
      ,qr_code
      ,purchase_amount
      ,net_amount
      ,redeem_deal
      ,deal_id
      ,deal_discount
      ,claim_status
      ,deal_record_id
      ,store_id
      ,is_cls_merchant
      ,txn_data
      ,redeem_lp
      ,burn_status
      ,burn_lp
      ,cls_burn_ref_no
      ,earn_status
      ,earned_lp
      ,cls_earn_ref_no
      ,transaction_start_date
      ,transaction_end_date
      ,transaction_expiry_date
      ,transaction_updated_date
      )
    VALUES (
      S.user_transaction_id
      ,S.aid
      ,S.current_state
      ,S.trigger_source
      ,S.merchant_id
      ,S.qr_code
      ,CAST(S.purchase_amount AS NUMERIC)
      ,CAST(S.net_amount AS NUMERIC)
      ,S.redeem_deal
      ,S.deal_id
      ,CAST(S.deal_discount AS NUMERIC)
      ,S.claim_status
      ,S.deal_record_id
      ,S.store_id
      ,S.is_cls_merchant
      ,S.txn_data
      ,S.redeem_lp
      ,S.burn_status
      ,CAST(S.burn_lp AS NUMERIC)
      ,S.cls_burn_ref_no
      ,S.earn_status
      ,S.earned_lp
      ,S.cls_earn_ref_no
      ,datetime(S.transaction_start_date)
      ,datetime(S.transaction_end_date)
      ,datetime(S.transaction_expiry_date)
      ,datetime(S.transaction_updated_date)
      );
