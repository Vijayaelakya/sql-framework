MERGE
  origins_factsql.mult_uom T
USING
  (SELECT DISTINCT
  barcodetype,
  srvdtstamp,
  dt_stamp,
  fuser,
  prodcode,
  barcode,
  id_prodmult,
  mprodcode   
FROM `ne-obfs-data-cloud-prod.data_staging.org_fact_fa_origins_healthcare_prodmult`) S
ON
T.id_prodmult = cast(S.id_prodmult as int)
WHEN MATCHED THEN UPDATE SET
 T.barcode_type = S.barcodetype,
 T.srvdtstamp = cast(S.srvdtstamp as datetime),
 T.dt_stamp = cast(S.dt_stamp as datetime),
 T.fuser = S.fuser,
 T.prodcode = S.prodcode,
 T.barcode = S.barcode,
 T.id_prodmult = cast(S.id_prodmult as int),
 T.mprodcode = S.mprodcode,
 T.last_update_dt = CURRENT_DATETIME()
  WHEN NOT MATCHED
  THEN
INSERT
(  barcode_type,
  srvdtstamp,
  dt_stamp,
  fuser,
  prodcode,
  barcode,
  id_prodmult,
  mprodcode,
  last_update_dt )
VALUES 
(
  S.barcodetype,
  cast(S.srvdtstamp as datetime),
  cast(S.dt_stamp as datetime),
  S.fuser,
  S.prodcode,
  S.barcode,
  cast(S.id_prodmult as int),
  S.mprodcode,
  CURRENT_DATETIME()
  )