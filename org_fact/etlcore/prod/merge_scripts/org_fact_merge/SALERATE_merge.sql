MERGE
  origins_factsql.sale_rate T
USING
  (SELECT DISTINCT
 cutprice4,
mrp_rate,
srvdtstamp,
fuser,
sequence,
schcode,
dt_stamp,
cutprice3,
cutprice2,
rate3,
cutprice1,
rate2,
to_dt,
rate4,
from_dt,
cutprice5,
rate1,
rate5,
id_salerate,
prodcode,
FROM `ne-obfs-data-cloud-prod.data_staging.org_fact_fa_origins_healthcare_salerate`) S
ON
T.id_salerate = cast(S.id_salerate as int)
WHEN MATCHED THEN UPDATE SET
T.cutprice4 = cast(S.cutprice4 as float64),
T.mrp_rate = cast(S.mrp_rate as float64),
T.srvdtstamp = cast(S.srvdtstamp as datetime),
T.fuser = cast(S.fuser as string),
T.sequence = cast(S.sequence as string),
T.schcode = cast(S.schcode as string),
T.dt_stamp = cast(S.dt_stamp as datetime),
T.cutprice3 = cast(S.cutprice3 as float64),
T.cutprice2 = cast(S.cutprice2 as float64),
T.rate3 = cast(S.rate3 as float64),
T.cutprice1 = cast(S.cutprice1 as float64),
T.rate2 = cast(S.rate2 as float64),
T.to_dt = cast(S.to_dt as datetime),
T.rate4 = cast(S.rate4 as float64),
T.from_dt = cast(S.from_dt as datetime),
T.cutprice5 = cast(S.cutprice5 as float64),
T.rate1 = cast(S.rate1 as float64),
T.rate5 = cast(S.rate5 as float64),
T.id_salerate = cast(S.id_salerate as integer),
T.prodcode = cast(S.prodcode as string)
  WHEN NOT MATCHED
  THEN
INSERT
(  
cutprice4,
mrp_rate,
srvdtstamp,
fuser,
sequence,
schcode,
dt_stamp,
cutprice3,
cutprice2,
rate3,
cutprice1,
rate2,
to_dt,
rate4,
from_dt,
cutprice5,
rate1,
rate5,
id_salerate,
prodcode
)
VALUES 
(
cast(S.cutprice4 as float64),
cast(S.mrp_rate as float64),
cast(S.srvdtstamp as datetime),
cast(S.fuser as string),
cast(S.sequence as string),
cast(S.schcode as string),
cast(S.dt_stamp as datetime),
cast(S.cutprice3 as float64),
cast(S.cutprice2 as float64),
cast(S.rate3 as float64),
cast(S.cutprice1 as float64),
cast(S.rate2 as float64),
cast(S.to_dt as datetime),
cast(S.rate4 as float64),
cast(S.from_dt as datetime),
cast(S.cutprice5 as float64),
cast(S.rate1 as float64),
cast(S.rate5 as float64),
cast(S.id_salerate as integer),
cast(S.prodcode as string)
  )