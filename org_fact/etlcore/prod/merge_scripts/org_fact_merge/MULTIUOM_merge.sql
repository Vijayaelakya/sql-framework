MERGE
  origins_factsql.pref_uom T
USING
  (select distinct 
schcode,
scheme,
sequence,
qty,
uom,
factor,
baseuom,
category,
fuser,
dt_stamp,
srvdtstamp
from `ne-obfs-data-cloud-prod.data_staging.org_fact_fa_origins_healthcare_multiuom` ) S
ON T.schcode = S.schcode
and T.sequence = S.sequence
and T.factor = cast(S.factor as float64)
and T.scheme = S.scheme
and T.qty = cast(S.qty as float64)
and T.uom = S.uom
and T.category = S.category
WHEN MATCHED THEN UPDATE SET
T.schcode = S.schcode,
T.scheme = S.scheme,
T.sequence = S.sequence,
T.qty = cast(S.qty as float64),
T.uom = S.uom,
T.factor = cast(S.factor as float64),
T.base_uom = S.baseuom,
T.category = S.category,
T.fuser = S.fuser,
T.dt_stamp = cast(S.dt_stamp as datetime),
T.srvdtstamp = cast(S.srvdtstamp as datetime),
T.last_update_dt = CURRENT_DATETIME()
  WHEN NOT MATCHED
  THEN
INSERT
( 
schcode,
scheme,
sequence,
qty,
uom,
factor,
base_uom,
category,
fuser,
dt_stamp,
srvdtstamp,
last_update_dt  )
VALUES 
(
S.schcode,
S.scheme,
S.sequence,
cast(S.qty as float64),
S.uom,
cast(S.factor as float64),
S.baseuom,
S.category,
S.fuser,
cast(S.dt_stamp as datetime),
cast(S.srvdtstamp as datetime),
CURRENT_DATETIME()
  )