BEGIN
DELETE FROM link.link_cls_product_code
WHERE ifnull(PRODUCT_CODE, '?^$@') in (select ifnull(PRODUCT_CODE, '?^$@') from staging.cls_blp_product_code);

INSERT INTO link.link_cls_product_code
SELECT DISTINCT club_code,
	    product_code,
	    description_english,
	    description_language2,
	    status,
	    date(timestamp(last_update_date)) as last_update_date,
	    last_update_by,
	    date(timestamp(last_approve_date)) as last_approve_date,
	    last_approve_by,
	    record_no,
	    business_id,
	    workflow_status,
	    seq_no,
	    date(timestamp(retention_date)) as retention_date
	from staging.cls_blp_product_code;

end;
