MERGE
  link_gcp.link_cls_txn_all T
USING
  (select 
a.csn, 
t.contributor_id,
t.corporate_id as corp_id, 
t.establishment_no, 
t.branch_no,
'' authorization_code, 
t.txn_category,
t.oe_pos_no, 
t.oe_batch_no, 
t.currency_code, 
t.reference_no as ref_no, 
t.transaction_ref_no as txn_ref_no, 
t.seq_no, 
t.scheme_id, 
t.rule_id, 
cast(t.point_adjusted as numeric) as point_val, 
cast(t.point_amount as numeric) as point_amount, 
cast(t.gross_transaction_amt as numeric) as gross_transaction_amt, 
cast(t.nett_transaction_amt as numeric) as nett_transaction_amt,
t.loyalty_account_no, 
t.club_code, 
t.pool_id, 
t.sub_pool_id, 
t.card_no, 
t.card_seq_no,
t.product_account_no, 
t.product_account_type, 
t.product_account_level,
timestamp(t.transaction_datetime) as txn_datetime, 
timestamp(t.post_date) as post_datetime, 
timestamp(t.settlement_datetime) as settlement_datetime, 
timestamp(t.auto_settlement_date) as auto_settlement_datetime,
timestamp(t.last_update_date) as last_update_datetime, 
date(timestamp(t.transaction_datetime)) as txn_date, 
date(timestamp(t.post_date)) as post_date, 
date(timestamp(t.settlement_datetime)) as settlement_date, 
date(timestamp(t.auto_settlement_date)) as auto_settlement_date,
date(timestamp(t.last_update_date)) as last_update_date, 
t.last_update_by, 
null quantity, 
'' invoice, 
'' receipt_no, 
t.stan,
t.transaction_code as txn_code, 
t.transaction_type as txn_type, 
t.cancellation_ind as cancel_ind, 
timestamp(t.cancellation_date) as cancel_datetime,
regexp_replace(t.reversal_reason,'[[:cntrl:]]','') as reversal_reason,
regexp_replace(t.remarks,'[[:cntrl:]]','') as remarks,
'' payment_method, 
t.record_type, 
t.business_id
from staging.cls_blp_adjustment_transaction t left join staging.cls_blp_loyalty_account a
on (trim(t.loyalty_account_no) = a.loyalty_account_no and trim(t.club_code)= a.club_code)) S
ON
				T.txn_type = S.txn_type
			and T.ref_no = S.ref_no
			and T.seq_no = S.seq_no
WHEN MATCHED THEN UPDATE SET 
T.csn = S.csn,
T.contributor_id = S.contributor_id,
T.corp_id = S.corp_id,
T.establishment_no = S.establishment_no,
T.branch_no = S.branch_no,
T.authorization_code = S.authorization_code,
T.txn_category = S.txn_category,
T.oe_pos_no = S.oe_pos_no,
T.oe_batch_no = S.oe_batch_no,
T.currency_code = S.currency_code,
T.ref_no = S.ref_no,
T.txn_ref_no = S.txn_ref_no,
T.seq_no = S.seq_no,
T.scheme_id = S.scheme_id,
T.rule_id = S.rule_id,
T.point_val = S.point_val,
T.point_amount = S.point_amount,
T.gross_transaction_amt = S.gross_transaction_amt,
T.nett_transaction_amt = S.nett_transaction_amt,
T.loyalty_account_no = S.loyalty_account_no,
T.club_code = S.club_code,
T.pool_id = S.pool_id,
T.sub_pool_id = S.sub_pool_id,
T.card_no = S.card_no,
T.card_seq_no = S.card_seq_no,
T.product_account_no = S.product_account_no,
T.product_account_type = S.product_account_type,
T.product_account_level = S.product_account_level,
T.txn_datetime = S.txn_datetime,
T.post_datetime = S.post_datetime,
T.settlement_datetime = S.settlement_datetime,
T.auto_settlement_datetime = S.auto_settlement_datetime,
T.last_update_datetime = S.last_update_datetime,
T.txn_date = S.txn_date,
T.post_date = S.post_date,
T.settlement_date = S.settlement_date,
T.auto_settlement_date = S.auto_settlement_date,
T.last_update_date = S.last_update_date,
T.last_update_by = S.last_update_by,
T.quantity = S.quantity,
T.invoice = S.invoice,
T.receipt_no = S.receipt_no,
T.stan = S.stan,
T.txn_code = S.txn_code,
T.txn_type = S.txn_type,
T.cancel_ind = S.cancel_ind,
T.cancel_datetime = S.cancel_datetime,
T.reversal_reason = S.reversal_reason,
T.remarks = S.remarks,
T.payment_method = S.payment_method,
T.record_type = S.record_type,
T.business_id = S.business_id
  WHEN NOT MATCHED
  THEN
INSERT
(csn,contributor_id,corp_id,establishment_no,branch_no,authorization_code,txn_category,oe_pos_no,oe_batch_no,currency_code,ref_no,txn_ref_no,seq_no,scheme_id,rule_id,point_val,point_amount,gross_transaction_amt,nett_transaction_amt,loyalty_account_no,club_code,pool_id,sub_pool_id,card_no,card_seq_no,product_account_no,product_account_type,product_account_level,txn_datetime,post_datetime,settlement_datetime,auto_settlement_datetime,last_update_datetime,txn_date,post_date,settlement_date,auto_settlement_date,last_update_date,last_update_by,quantity,invoice,receipt_no,stan,txn_code,txn_type,cancel_ind,cancel_datetime,reversal_reason,remarks,payment_method,record_type,business_id)
VALUES (S.csn,S.contributor_id,S.corp_id,S.establishment_no,S.branch_no,S.authorization_code,S.txn_category,S.oe_pos_no,S.oe_batch_no,S.currency_code,S.ref_no,S.txn_ref_no,S.seq_no,S.scheme_id,S.rule_id,S.point_val,S.point_amount,S.gross_transaction_amt,S.nett_transaction_amt,S.loyalty_account_no,S.club_code,S.pool_id,S.sub_pool_id,S.card_no,S.card_seq_no,S.product_account_no,S.product_account_type,S.product_account_level,S.txn_datetime,S.post_datetime,S.settlement_datetime,S.auto_settlement_datetime,S.last_update_datetime,S.txn_date,S.post_date,S.settlement_date,S.auto_settlement_date,S.last_update_date,S.last_update_by,S.quantity,S.invoice,S.receipt_no,S.stan,S.txn_code,S.txn_type,S.cancel_ind,S.cancel_datetime,S.reversal_reason,S.remarks,S.payment_method,S.record_type,S.business_id)
