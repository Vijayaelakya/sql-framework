begin
	delete from link_gcp.link_cls_loyalty_account_extension where true;

	insert into link_gcp.link_cls_loyalty_account_extension 
	select distinct 
	csn,
	branch,
	department,
	main_union,
	link_membership_type,
	business_id
	from staging.cls_blp_loyalty_account_extension;
end;