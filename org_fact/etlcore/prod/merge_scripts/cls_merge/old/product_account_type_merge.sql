MERGE
  link_gcp.link_cls_product_account_type T
USING
  (select distinct 
		product_account_type, 
	    description_english, 
	    description_language2, 
	    status, 
	    date(timestamp(last_update_date)) as last_update_date, 
	    last_update_by, 
	    date(timestamp(last_approve_date)) as last_approve_date, 
	    last_approve_by, 
	    pos_reference_no, 
	    card_selection_seq, 
	    record_no, 
	    business_id, 
	    batch_id, 
	    batch_no, 
	    currency_code, 
	    product_group, 
	    product_group_description, 
	    remarks, 
	    date(timestamp(retention_date)) as retention_date
	from staging.cls_blp_product_account_type) S
ON
ifnull(T.product_account_type, '?^$@') = ifnull(S.product_account_type, '?^$@')
WHEN MATCHED THEN UPDATE SET 
T.product_account_type = S.product_account_type,
T.description_english = S.description_english,
T.description_language2 = S.description_language2,
T.status = S.status,
T.last_update_date = S.last_update_date,
T.last_update_by = S.last_update_by,
T.last_approve_date = S.last_approve_date,
T.last_approve_by = S.last_approve_by,
T.pos_reference_no = S.pos_reference_no,
T.card_selection_seq = S.card_selection_seq,
T.record_no = S.record_no,
T.business_id = S.business_id,
T.batch_id = S.batch_id,
T.batch_no = S.batch_no,
T.currency_code = S.currency_code,
T.product_group = S.product_group,
T.product_group_description = S.product_group_description,
T.remarks = S.remarks,
T.retention_date = S.retention_date
  WHEN NOT MATCHED
  THEN
INSERT
(product_account_type,description_english,description_language2,status,last_update_date,last_update_by,last_approve_date,last_approve_by,pos_reference_no,card_selection_seq,record_no,business_id,batch_id,batch_no,currency_code,product_group,product_group_description,remarks,retention_date)
VALUES (S.product_account_type,S.description_english,S.description_language2,S.status,S.last_update_date,S.last_update_by,S.last_approve_date,S.last_approve_by,S.pos_reference_no,S.card_selection_seq,S.record_no,S.business_id,S.batch_id,S.batch_no,S.currency_code,S.product_group,S.product_group_description,S.remarks,S.retention_date)