MERGE
  link_gcp.link_cls_card T
USING
  (
  select distinct 
		stage.card_no,
		stage.seq_no,
		stage.card_status,
		stage.card_type,
		stage.csn,
		stage.customer_id,
		stage.reference_no,
		stage.reason_code,
		stage.channel_create,
		date(timestamp(stage.issue_date)) as issue_date,
		date(timestamp(stage.activation_date)) as activation_date,
		date(timestamp(stage.expiry_date)) as expiry_date,
		date(timestamp(stage.renewal_date)) as renewal_date,
		date(timestamp(stage.lif_upload_date)) as lif_upload_date,
		stage.status,
		stage.last_update_by as last_upd_by,
		timestamp(stage.last_update_date) as last_upd_date,
		stage.home_branch
	from staging.cls_blp_card stage,
	(
	select card_no, seq_no, max(last_update_date) max_last_upd_date
	from staging.cls_blp_card
	group by card_no, seq_no
	) x
	where stage.card_no = x.card_no
	and stage.seq_no = x.seq_no
	and stage.last_update_date = x.max_last_upd_date) S
ON
  ifnull(T.card_no, '?^$@') = ifnull(S.card_no, '?^$@')
	and ifnull(T.seq_no, '?^$@') = ifnull(S.seq_no, '?^$@')
  WHEN MATCHED THEN UPDATE SET T.card_no = S.card_no,
T.seq_no = S.seq_no,
T.card_status = S.card_status,
T.card_type = S.card_type,
T.csn = S.csn,
T.customer_id = S.customer_id,
T.reference_no = S.reference_no,
T.reason_code = S.reason_code,
T.channel_create = S.channel_create,
T.issue_date = S.issue_date,
T.activation_date = S.activation_date,
T.expiry_date = S.expiry_date,
T.renewal_date = S.renewal_date,
T.lif_upload_date = S.lif_upload_date,
T.status = S.status,
T.last_upd_by = S.last_upd_by,
T.last_upd_date = S.last_upd_date,
T.home_branch = S.home_branch
  WHEN NOT MATCHED
  THEN
INSERT
  (card_no,seq_no,card_status,card_type,csn,customer_id,reference_no,reason_code,channel_create,issue_date,activation_date,expiry_date,renewal_date,lif_upload_date,status,last_upd_by,last_upd_date,home_branch)
VALUES (S.card_no,S.seq_no,S.card_status,S.card_type,S.csn,S.customer_id,S.reference_no,S.reason_code,S.channel_create,S.issue_date,S.activation_date,S.expiry_date,S.renewal_date,S.lif_upload_date,S.status,S.last_upd_by,S.last_upd_date,S.home_branch)