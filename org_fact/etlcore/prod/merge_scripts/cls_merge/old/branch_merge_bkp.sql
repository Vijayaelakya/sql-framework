MERGE
  link_gcp.link_cls_branch T
USING
  (SELECT DISTINCT 
	stage.BRANCH_NO,
	stage.ESTABLISHMENT_NO,
	stage.CORPORATE_ID,
	REGEXP_REPLACE(stage.BRANCH_NAME_ENGLISH_1,'[[:cntrl:]]','') AS BRANCH_NAME_ENGLISH_1,
	REGEXP_REPLACE(stage.BRANCH_NAME_ENGLISH_2,'[[:cntrl:]]','') AS BRANCH_NAME_ENGLISH_2,
	BRANCH_STATUS,
	date(timestamp(SERVICE_START_DATE)) as SERVICE_START_DATE,
	date(timestamp(SERVICE_TERMINATION_DATE)) as SERVICE_TERMINATION_DATE,
	REGEXP_REPLACE
	(
		CASE 
			WHEN LENGTH(TRIM(ADDRESS_1))>0 OR LENGTH(TRIM(ADDRESS_2))>0 
				THEN ADDRESS_1||';'||ADDRESS_2 
			ELSE '' 
		END, 
	'[[:cntrl:]]',''
	) AS BRANCH_ADDR,
	POSTAL_CODE AS POSTCODE,
	STATUS,
	LAST_UPDATE_BY AS LAST_UPD_BY,
	timestamp(LAST_UPDATE_DATE) AS LAST_UPD_DATE,
	timestamp(LAST_APPROVE_DATE) AS LAST_APPROVE_DATE
	FROM staging.cls_blp_branch stage,
	(
	SELECT BRANCH_NO, MAX(LAST_UPDATE_DATE) MAX_LAST_UPD_DATE
	FROM staging.cls_blp_branch
	GROUP BY BRANCH_NO
	) X
	WHERE stage.BRANCH_NO = X.BRANCH_NO
	AND stage.LAST_UPDATE_DATE = X.MAX_LAST_UPD_DATE) S
ON
ifnull(T.branch_no, -999) = ifnull(S.branch_no, -999)
  WHEN MATCHED THEN UPDATE SET 
T.branch_no = S.branch_no,
T.establishment_no = S.establishment_no,
T.corporate_id = S.corporate_id,
T.branch_name_english_1 = S.branch_name_english_1,
T.branch_name_english_2 = S.branch_name_english_2,
T.branch_status = S.branch_status,
T.service_start_date = S.service_start_date,
T.service_termination_date = S.service_termination_date,
T.branch_addr = S.branch_addr,
T.postcode = S.postcode,
T.status = S.status,
T.last_upd_by = S.last_upd_by,
T.last_upd_date = S.last_upd_date
  WHEN NOT MATCHED
  THEN
INSERT
(branch_no,establishment_no,corporate_id,branch_name_english_1,branch_name_english_2,branch_status,service_start_date,service_termination_date,branch_addr,postcode,status,last_upd_by,last_upd_date,LAST_APPROVE_DATE)
VALUES (S.branch_no,S.establishment_no,S.corporate_id,S.branch_name_english_1,S.branch_name_english_2,S.branch_status,S.service_start_date,S.service_termination_date,S.branch_addr,S.postcode,S.status,S.last_upd_by,S.last_upd_date, S.LAST_APPROVE_DATE)
