begin
	delete from link_gcp.link_cls_terminal_scheme_pool where true;

	insert into link_gcp.link_cls_terminal_scheme_pool 
		select distinct 
		corporate_id,
		establishment_no,
		branch_no,
		oe_pos_no,
		club_code,
		scheme_id,
		pool_id,
		status,
		last_update_by,
		timestamp(last_update_date) as last_update_date,
		business_id
	from staging.cls_blp_terminal_scheme_pool;
end;