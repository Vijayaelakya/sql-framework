BEGIN

DELETE FROM link_gcp.link_cls_scheme_rule_master
WHERE ifnull(SCHEME_ID, '?^$@') in (select ifnull(SCHEME_ID, '?^$@') from staging.cls_blp_scheme_rule_master);

INSERT INTO link_gcp.link_cls_scheme_rule_master
SELECT DISTINCT club_code,
		scheme_id,
		cast(min_pts_award as numeric) as min_pts_award,
		cast(max_pts_award as numeric) as max_pts_award,
		rounding_type,
		tran_amt_type,
		pre_processing_rule,
		post_processing_rule,
		status,
		workflow_status,
		date(timestamp(last_update_date)) as last_update_date,
		last_update_by,
		date(timestamp(last_approve_date)) as last_approve_date,
		last_approve_by,
		seq,
		scheme_counter_id,
		cast(cap_amount as numeric) as cap_amount,
		record_no,
		business_id,
		campaign_id,
		absolute_amt,
		post_process_scheme_counter_id,
		date(timestamp(retention_date)) as retention_date,
		scheme_counter,
		status_code
	from staging.cls_blp_scheme_rule_master;
End;
