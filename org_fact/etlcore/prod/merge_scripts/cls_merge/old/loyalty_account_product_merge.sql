BEGIN
DELETE FROM link_gcp.link_cls_loyalty_account_product
WHERE (ifnull(LOYALTY_ACCOUNT_NO, '?^$@'),
ifnull(PRODUCT_ACCOUNT_NO, '?^$@'),
ifnull(CLUB_CODE, '?^$@')) in
(Select (ifnull(LOYALTY_ACCOUNT_NO, '?^$@'),ifnull(PRODUCT_ACCOUNT_NO, '?^$@'), ifnull(CLUB_CODE, '?^$@')) FROM staging.cls_blp_loyalty_account_product);

INSERT INTO link_gcp.link_cls_loyalty_account_product
select distinct
	stage.loyalty_account_no,
	stage.product_account_no,
	stage.club_code,
	stage.product_account_type,
	date(timestamp(stage.termination_date)) as termination_date,
	stage.status,
	stage.last_update_by as last_upd_by,
	timestamp(stage.last_update_date) as last_upd_date
	from staging.cls_blp_loyalty_account_product stage,
	(
	select loyalty_account_no, product_account_no, club_code, max(last_update_date) max_last_upd_date
	from staging.cls_blp_loyalty_account_product
	group by loyalty_account_no, product_account_no, club_code
	) x
	where stage.loyalty_account_no = x.loyalty_account_no
	and stage.product_account_no = x.product_account_no
	and stage.club_code = x.club_code
	and stage.last_update_date = x.max_last_upd_date;
End;

