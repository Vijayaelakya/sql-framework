MERGE
  link_gcp.link_cls_corporation T
USING
  (select distinct 
	stage.corporate_id,
	REGEXP_REPLACE(stage.corporate_name_english_1,'[[:cntrl:]]','') as corporate_name_english_1,
	REGEXP_REPLACE(stage.corporate_name_english_2,'[[:cntrl:]]','') as corporate_name_english_2,
	date(timestamp(stage.date_joined))as date_joined,
	date(timestamp(stage.service_start_date)) as service_start_date,
	date(timestamp(stage.service_termination_date)) as service_termination_date,
	stage.status,
	stage.last_update_by as last_upd_by,
	timestamp(stage.last_update_date) as last_upd_date
	from `staging.cls_blp_corporation` stage,
	(
		select corporate_id, max(last_update_date) max_last_upd_date
		from staging.cls_blp_corporation
		group by corporate_id
	) x
	where stage.corporate_id = x.corporate_id
	and stage.last_update_date = x.max_last_upd_date) S
ON
ifnull(T.corporate_id, '?^$@') = ifnull(S.corporate_id, '?^$@')
  WHEN MATCHED THEN UPDATE SET 
T.corporate_id = S.corporate_id,
T.corporate_name_english_1 = S.corporate_name_english_1,
T.corporate_name_english_2 = S.corporate_name_english_2,
T.date_joined = S.date_joined,
T.service_start_date = S.service_start_date,
T.service_termination_date = S.service_termination_date,
T.status = S.status,
T.last_upd_by = S.last_upd_by,
T.last_upd_date = S.last_upd_date
  WHEN NOT MATCHED
  THEN
INSERT
(corporate_id,corporate_name_english_1,corporate_name_english_2,date_joined,service_start_date,service_termination_date,status,last_upd_by,last_upd_date)
VALUES (S.corporate_id,S.corporate_name_english_1,S.corporate_name_english_2,S.date_joined,S.service_start_date,S.service_termination_date,S.status,S.last_upd_by,S.last_upd_date)