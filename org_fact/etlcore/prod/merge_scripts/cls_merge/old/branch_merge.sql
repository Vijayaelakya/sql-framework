BEGIN
-- Create temp table tmptable as select * from staging.cls_blp_branch;

DELETE from link_gcp.link_cls_branch where ifnull(BRANCH_NO, -999) in (select distinct ifnull(stage.BRANCH_NO, -999) from staging.cls_blp_branch stage);

INSERT INTO link_gcp.link_cls_branch
SELECT DISTINCT
	stage.BRANCH_NO,
	stage.ESTABLISHMENT_NO,
	stage.CORPORATE_ID,
	REGEXP_REPLACE(stage.BRANCH_NAME_ENGLISH_1,'[[:cntrl:]]','') AS BRANCH_NAME_ENGLISH_1,
	REGEXP_REPLACE(stage.BRANCH_NAME_ENGLISH_2,'[[:cntrl:]]','') AS BRANCH_NAME_ENGLISH_2,
	BRANCH_STATUS,
	date(timestamp(SERVICE_START_DATE)) as SERVICE_START_DATE,
	date(timestamp(SERVICE_TERMINATION_DATE)) as SERVICE_TERMINATION_DATE,
	REGEXP_REPLACE
	(
		CASE
			WHEN LENGTH(TRIM(ADDRESS_1))>0 OR LENGTH(TRIM(ADDRESS_2))>0
				THEN ADDRESS_1||';'||ADDRESS_2
			ELSE ''
		END,
	'[[:cntrl:]]',''
	) AS BRANCH_ADDR,
	POSTAL_CODE AS POSTCODE,
	STATUS,
	LAST_UPDATE_BY AS LAST_UPD_BY,
	timestamp(LAST_UPDATE_DATE) AS LAST_UPD_DATE,
	timestamp(LAST_APPROVE_DATE) AS LAST_APPROVE_DATE
FROM staging.cls_blp_branch stage,
(
SELECT BRANCH_NO, MAX(LAST_UPDATE_DATE) MAX_LAST_UPD_DATE
FROM staging.cls_blp_branch
GROUP BY BRANCH_NO
) X
WHERE stage.BRANCH_NO = X.BRANCH_NO
AND stage.LAST_UPDATE_DATE = X.MAX_LAST_UPD_DATE;

END;
