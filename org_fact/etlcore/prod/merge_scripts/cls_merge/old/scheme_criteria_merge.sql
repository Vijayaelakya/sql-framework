BEGIN

DELETE FROM link_gcp.link_cls_scheme_criteria
WHERE ifnull(SCHEME_ID, '?^$@') in (select ifnull(SCHEME_ID, '?^$@') from staging.cls_blp_scheme_criteria);

INSERT INTO link_gcp.link_cls_scheme_criteria
SELECT DISTINCT branch_group
	,branch_sub_group
	,club_code
	,counter_1_value
	,counter_2_value
	,counter_3_value
	,days_of_week
	,dob_ind
	,dom_anniversary_ind
	,dow_ind
	,date(timestamp(end_time)) as end_time
	,establishment_group
	,establishment_sub_group
	,exc_branch
	,exc_card_type
	,exc_cli_attributes
	,exc_cli_spare_attr
	,exc_currency_code
	,exc_dept_code
	,exc_establishment
	,exc_la_attributes
	,exc_la_class
	,exc_la_group
	,exc_la_segment
	,exc_la_spare_attr
	,exc_la_status
	,exc_la_tier
	,exc_la_type
	,exc_payment_method
	,exc_pos_id
	,exc_postal_code
	,exc_product_code
	,exc_product_group
	,exc_product_range
	,exc_race_code
	,exc_source_system
	,gender
	,inc_birth_place
	,inc_branch
	,inc_card_type
	,inc_cli_attributes
	,inc_cli_spare_attr
	,inc_corporation
	,inc_country
	,inc_currency_code
	,inc_dept_code
	,inc_district
	,inc_establishment
	,inc_la_attributes
	,inc_la_class
	,inc_la_group
	,inc_la_segment
	,inc_la_spare_attr
	,inc_la_status
	,inc_la_tier
	,inc_la_type
	,inc_nationality
	,inc_occupation
	,inc_payment_method
	,inc_pos_id
	,inc_postal_code
	,inc_product_code
	,inc_product_group
	,inc_product_range
	,inc_race_code
	,inc_religion
	,inc_source_system
	,inc_state
	,cast(min_point as numeric) as min_point
	,cast(min_txn_amt as numeric) as min_txn_amt
	,txn_amt_type
	,mob_ind
	,mom_anniversary_ind
	,mow_ind
	,pos_group
	,pos_sub_group
	,scheme_id
	,date(timestamp(start_time)) as start_time
	,years_of_membership
	,ext_function
	,fq_prev_cur_period
	,fq_period_criteria
	,fq_no_period
	,cast(fq_value as numeric) as fq_value
	,val_prev_cur_period
	,val_period_criteria
	,val_no_period
	,cast(val_value as numeric) as val_value
	,max_period_type_last_txn
	,max_period_val_last_txn
	,dojoiningind
	,mojoiningind
	,criteria_param_bitmap
	,status
	,date(timestamp(last_approve_date)) as last_approve_date
	,last_approve_by
	,date(timestamp(last_update_date)) as last_update_date
	,last_update_by
	,fq_scheme_id
	,val_scheme_id
	,txn_criteria_op
	,cast(txn_criteria_value as numeric) as txn_criteria_value
	,pool_id
	,transaction_code
	,nett_txn_amt_op
	,cast(nett_txn_amt_value as numeric) as nett_txn_amt_value
	,cast(min_nett_txn_amt as numeric) as min_nett_txn_amt
	,pa_setup_date_type
	,pa_setup_date_period
	,inc_corporation_group
	,exc_corporation
	,exc_corporation_group
	,exc_establishment_group
	,exc_establishment_sub_group
	,exc_branch_group
	,exc_branch_sub_group
	,exc_pos_group
	,exc_pos_sub_group
	,pool_bal_id
	,pool_bal_op
	,cast(pool_bal_value as numeric) as pool_bal_value
	,wk_dob_type
	,mth_wk_dob
	,mth_wk_dob_type
	,moyr_on_book_ind
	,yr_on_book_period
	,yr_on_book_value
	,cfd_days
	,yr_on_book_days
	,exc_la_class_pref
	,exc_la_group_pref
	,exc_la_segment_pref
	,exc_la_status_pref
	,exc_la_tier_pref
	,exc_la_type_pref
	,inc_la_class_pref
	,inc_la_group_pref
	,inc_la_segment_pref
	,inc_la_status_pref
	,inc_la_tier_pref
	,inc_la_type_pref
	,inc_product_account_type_ch
	,exc_product_account_type_ch
	,inc_promotion_code
	,inc_card_status
	,record_no
	,business_id
	,pa_setup_date_operator
	,last_txn_operator
	,years_of_membership_operator
	,pool_txn_amt_type
	,pool_operator
	,pool_id_2
	,campaign_id
	,acct_tenure_end_range
	,acct_tenure_start_range
	,date(timestamp(aod_end_date)) as aod_end_date
	,aod_op
	,date(timestamp(aod_start_date)) as aod_start_date
	,wk_dow_type
	,card_block_code
	,cashback_opt
	,credit_limit
	,credit_limit_op
	,dba_name_group
	,dba_name_group_op
	,exc_mcc_code
	,exc_sic_code
	,inc_mcc_code
	,inc_sic_code
	,lp_txn_date_op_1
	,lp_txn_date_op_2
	,lp_txn_date_op_3
	,max_award_nmonths
	,max_no_of_awards
	,mia
	,mia_active_pa
	,mia_active_pa_op
	,mia_op
	,no_block_card_days
	,num_active_cards
	,num_active_cards_op
	,overdue_ind
	,pa_amount_1
	,pa_amount_1_op
	,pa_amount_2
	,pa_amount_2_op
	,pa_amount_3
	,pa_amount_3_op
	,pa_amount_4
	,pa_amount_4_op
	,pa_amount_5
	,pa_amount_5_op
	,pa_criteria_1
	,pa_criteria_1_op
	,pa_criteria_2
	,pa_criteria_2_op
	,pa_criteria_3
	,pa_criteria_3_op
	,pa_criteria_4
	,pa_criteria_4_op
	,pa_criteria_5
	,pa_criteria_5_op
	,pa_rel_code_1
	,pa_rel_code_1_op
	,pa_rel_code_2
	,pa_rel_code_2_op
	,pa_rel_code_3
	,pa_rel_code_3_op
	,pa_rel_code_4
	,pa_rel_code_4_op
	,pa_rel_code_5
	,pa_rel_code_5_op
	,date(timestamp(pa_setup_end_date)) as pa_setup_end_date
	,date(timestamp(pa_setup_start_date)) as pa_setup_start_date
	,pp_status
	,prime_plan
	,prime_plan_op
	,date(timestamp(retention_date)) as retention_date
	,segment_code
	,segment_code_op
	,service_rm_code
	,service_rm_code_op
	,source_code
	,source_code_op
	,tc_date_month
	,tc_date_month_op
	from staging.cls_blp_scheme_criteria;
end;
