MERGE
  link_gcp.link_cls_scheme T
USING
  (
  select distinct 
	scheme_id,
	cast(tran_count as int64) base_txn_ind,    
	cast(amount_multiplier as numeric) as amount_multiplier,
	cast(description_language2 as int64) as flag,
	club_code as scheme_club_code,
	pool_id as scheme_pool_id,
	date(timestamp(scheme_start_date)) as scheme_start_date,
	date(timestamp(scheme_end_date)) as scheme_end_date,
	description_english as scheme_description_english,
	status as scheme_status,
	cast(point_multiplier as numeric) as scheme_point_multiplier
	from 
	(select s.*,  
	row_number() over (partition by scheme_id order by last_update_date desc, status) rn 
	from staging.cls_blp_scheme s)
	where rn=1
	and
	(
	description_language2 is null
	or
	regexp_contains(trim(description_language2), '^[^a-za-z]*$')
	)
	and scheme_id is not null
  and tran_count is not null
	and amount_multiplier is not null
  and description_language2 is not null) S
ON
ifnull(T.scheme_id, '?^$@') = ifnull(S.scheme_id, '?^$@')
  WHEN MATCHED THEN UPDATE SET T.scheme_id = S.scheme_id,
T.base_txn_ind = S.base_txn_ind,
T.amount_multiplier = S.amount_multiplier,
T.flag = S.flag,
T.scheme_club_code = S.scheme_club_code,
T.scheme_pool_id = S.scheme_pool_id,
T.scheme_start_date = S.scheme_start_date,
T.scheme_end_date = S.scheme_end_date,
T.scheme_description_english = S.scheme_description_english,
T.scheme_status = S.scheme_status,
T.scheme_point_multiplier = S.scheme_point_multiplier
  WHEN NOT MATCHED
  THEN
INSERT
(scheme_id,base_txn_ind,amount_multiplier,flag,scheme_club_code,scheme_pool_id,scheme_start_date,scheme_end_date,scheme_description_english,scheme_status,scheme_point_multiplier)
VALUES (S.scheme_id,S.base_txn_ind,S.amount_multiplier,S.flag,S.scheme_club_code,S.scheme_pool_id,S.scheme_start_date,S.scheme_end_date,S.scheme_description_english,S.scheme_status,S.scheme_point_multiplier)