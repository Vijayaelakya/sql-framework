MERGE
  link_gcp.link_cls_attributes T
USING
  (select distinct 
	    attribute_no, 
	    attribute_shortname, 
	    attribute_type, 
	    id_level, 
	    seq, 
	    external_reference_no, 
	    description_english, 
	    description_language2, 
	    status, 
	    date(timestamp(last_update_date)) as last_update_date, 
	    last_update_by, 
	    date(timestamp(last_approve_date)) as last_approve_date, 
	    last_approve_by, 
	    record_no, 
	    business_id, 
	    date(timestamp(retention_date)) as retention_date, 
	    attribute_value, 
	    workflow_status, 
	    status_code, 
	    seq_no, 
	    attribute_id, 
	    data_type, 
	    attribute_update_by, 
	    attribute_group_code 
	from staging.cls_blp_attributes) S
ON
ifnull(T.attribute_no, -9999) = ifnull(S.attribute_no, -9999)
WHEN MATCHED THEN UPDATE SET 
T.attribute_no = S.attribute_no,
T.attribute_shortname = S.attribute_shortname,
T.attribute_type = S.attribute_type,
T.id_level = S.id_level,
T.seq = S.seq,
T.external_reference_no = S.external_reference_no,
T.description_english = S.description_english,
T.description_language2 = S.description_language2,
T.status = S.status,
T.last_update_date = S.last_update_date,
T.last_update_by = S.last_update_by,
T.last_approve_date = S.last_approve_date,
T.last_approve_by = S.last_approve_by,
T.record_no = S.record_no,
T.business_id = S.business_id,
T.retention_date = S.retention_date,
T.attribute_value = S.attribute_value,
T.workflow_status = S.workflow_status,
T.status_code = S.status_code,
T.seq_no = S.seq_no,
T.attribute_id = S.attribute_id,
T.data_type = S.data_type,
T.attribute_update_by = S.attribute_update_by,
T.attribute_group_code = S.attribute_group_code
  WHEN NOT MATCHED
  THEN
INSERT
(attribute_no,attribute_shortname,attribute_type,id_level,seq,external_reference_no,description_english,description_language2,status,last_update_date,last_update_by,last_approve_date,last_approve_by,record_no,business_id,retention_date,attribute_value,workflow_status,status_code,seq_no,attribute_id,data_type,attribute_update_by,attribute_group_code)
VALUES (S.attribute_no,S.attribute_shortname,S.attribute_type,S.id_level,S.seq,S.external_reference_no,S.description_english,S.description_language2,S.status,S.last_update_date,S.last_update_by,S.last_approve_date,S.last_approve_by,S.record_no,S.business_id,S.retention_date,S.attribute_value,S.workflow_status,S.status_code,S.seq_no,S.attribute_id,S.data_type,S.attribute_update_by,S.attribute_group_code)