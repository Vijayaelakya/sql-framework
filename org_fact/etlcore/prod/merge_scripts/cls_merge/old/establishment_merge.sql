MERGE
  link_gcp.link_cls_establishment T
USING
  (select distinct 
	stage.establishment_no,
	stage.corporate_id,
	regexp_replace(stage.establishment_name_english_1,'[[:cntrl:]]','') as establishment_name_english_1,
	regexp_replace(stage.establishment_name_english_2,'[[:cntrl:]]','') as establishment_name_english_2,
	stage.establishment_status,
	stage.establishment_type,
	date(timestamp(stage.service_start_date)) as service_start_date,
	date(timestamp(stage.service_termination_date)) as service_termination_date,
	stage.status,
	last_update_by as last_upd_by,
	timestamp(stage.last_update_date) as last_upd_date 
	from `staging.cls_blp_establishment` stage,
	(
	select establishment_no, max(last_update_date) max_last_upd_date
	from staging.cls_blp_establishment
	group by establishment_no
	) x
	where stage.establishment_no = x.establishment_no
	and stage.last_update_date = x.max_last_upd_date) S
ON
ifnull(T.establishment_no, -999) = ifnull(S.establishment_no, -999)
WHEN MATCHED THEN UPDATE SET 
T.establishment_no = S.establishment_no,
T.corporate_id = S.corporate_id,
T.establishment_name_english_1 = S.establishment_name_english_1,
T.establishment_name_english_2 = S.establishment_name_english_2,
T.establishment_status = S.establishment_status,
T.establishment_type = S.establishment_type,
T.service_start_date = S.service_start_date,
T.service_termination_date = S.service_termination_date,
T.status = S.status,
T.last_upd_by = S.last_upd_by,
T.last_upd_date = S.last_upd_date
  WHEN NOT MATCHED
  THEN
INSERT
(establishment_no,corporate_id,establishment_name_english_1,establishment_name_english_2,establishment_status,establishment_type,service_start_date,service_termination_date,status,last_upd_by,last_upd_date)
VALUES (S.establishment_no,S.corporate_id,S.establishment_name_english_1,S.establishment_name_english_2,S.establishment_status,S.establishment_type,S.service_start_date,S.service_termination_date,S.status,S.last_upd_by,S.last_upd_date)