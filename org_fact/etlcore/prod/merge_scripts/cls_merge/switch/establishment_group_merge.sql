BEGIN
DELETE FROM link.link_cls_establishment_group
WHERE ifnull(ESTABLISHMENT_GROUP, '?^$@') in (Select distinct ifnull(ESTABLISHMENT_GROUP, '?^$@') from staging.cls_blp_establishment_group);

INSERT INTO link.link_cls_establishment_group
select distinct
	establishment_group,
	    description_english,
	    description_language2,
	    status,
	    date(timestamp(last_update_date)) as last_update_date,
	    last_update_by,
	    date(timestamp(last_approve_date)) as last_approve_date,
	    last_approve_by,
	    record_no,
	    business_id,
	    report_indicator,
	    date(timestamp(retention_date)) as retention_date,
	    seq_no,
	    workflow_status
	from staging.cls_blp_establishment_group;
end;
