BEGIN
DELETE FROM link.link_cls_loyalty_account_group
WHERE ifnull(LOYALTY_ACCOUNT_GROUP, '?^$@') in (select distinct ifnull(LOYALTY_ACCOUNT_GROUP, '?^$@') from staging.cls_blp_loyalty_account_group);

INSERT INTO link.link_cls_loyalty_account_group
select distinct
	    club_code,
	    loyalty_account_group,
	    pos_reference_no,
	    description_english,
	    description_language2,
	    status,
	    date(timestamp(last_update_date)) as last_update_date,
	    last_update_by,
	    date(timestamp(last_approve_date)) as last_approve_date,
	    last_approve_by,
	    record_no,
	    business_id,
	    date(timestamp(retention_date)) as retention_date
	from staging.cls_blp_loyalty_account_group;
END;
