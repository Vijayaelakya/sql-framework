MERGE
  link.link_cls_eft_pos_group T
USING
  (select distinct 
	  eft_pos_group, 
	  description_english, 
	  description_language2, 
	  status, 
	  date(timestamp(last_update_date)) as last_update_date, 
	  last_update_by, 
	  date(timestamp(last_approve_date)) as last_approve_date, 
	  last_approve_by, 
	  pos_reference_no, 
	  record_no, 
	  business_id, 
	  date(timestamp(retention_date)) as retention_date
	from staging.cls_blp_eft_pos_group) S
ON
ifnull(T.eft_pos_group, '?^$@') = ifnull(S.eft_pos_group, '?^$@')
WHEN MATCHED THEN UPDATE SET 
T.eft_pos_group = S.eft_pos_group,
T.description_english = S.description_english,
T.description_language2 = S.description_language2,
T.status = S.status,
T.last_update_date = S.last_update_date,
T.last_update_by = S.last_update_by,
T.last_approve_date = S.last_approve_date,
T.last_approve_by = S.last_approve_by,
T.pos_reference_no = S.pos_reference_no,
T.record_no = S.record_no,
T.business_id = S.business_id,
T.retention_date = S.retention_date
  WHEN NOT MATCHED
  THEN
INSERT
(eft_pos_group,description_english,description_language2,status,last_update_date,last_update_by,last_approve_date,last_approve_by,pos_reference_no,record_no,business_id,retention_date)
VALUES (S.eft_pos_group,S.description_english,S.description_language2,S.status,S.last_update_date,S.last_update_by,S.last_approve_date,S.last_approve_by,S.pos_reference_no,S.record_no,S.business_id,S.retention_date)
