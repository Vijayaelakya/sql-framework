
BEGIN
DELETE FROM link.link_cls_scheme_counter
WHERE ifnull(SCHEME_COUNTER_ID, '?^$@') in (Select ifnull(SCHEME_COUNTER_ID, '?^$@') from staging.cls_blp_scheme_counter);

INSERT INTO link.link_cls_scheme_counter
SELECT DISTINCT scheme_counter_id,
	description_english,
	description_language2,
	scheme_counter_method_id,
	cast(reset_value as numeric) as reset_value,
	seq_no,
	status,
	workflow_status,
	asp_id,
	last_update_by,
	date(timestamp(last_update_date)) as last_update_date,
	last_approve_by,
	date(timestamp(last_approve_date)) as last_approve_date,
	month_interval,
	cast(cap_amount as numeric) as cap_amount,
	date(timestamp(counter_start_date)) as counter_start_date,
	record_no,
	business_id,
	counter_type,
	club_code,
	date(timestamp(retention_date)) as retention_date,
	status_code
	from staging.cls_blp_scheme_counter;
END;
