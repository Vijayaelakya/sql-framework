MERGE
  link.link_cls_client T
USING
  (
SELECT  distinct
  timestamp(application_date) as application_date
  ,birthplace
  ,citizen
  ,client_id_1 as client_id_1_sha1
  ,client_id_1 as client_id_1_sha256
  ,client_id_2 as client_id_2_sha1
  ,client_id_2 as client_id_2_sha256
  ,client_name_english as client_name_english_hash
  ,company_name_1
  ,company_name_2
  ,country_code
  ,csn
  ,designation
  ,timestamp(dob) as dob
  ,external_reference_no as external_reference_no_hash
  ,first_name_english as first_name_english_hash
  ,gender_code
  ,id_type_1
  ,id_type_2
  ,last_name_english as last_name_english_hash
  ,middle_name_english as middle_name_english_hash
  ,name_suffix_english as name_suffix_english_hash
  ,timestamp(occasion_date_1) as occasion_date_1
  ,timestamp(occasion_date_2) as occasion_date_2
  ,timestamp(occasion_date_3) as occasion_date_3
  ,occasion_name_1
  ,occasion_name_2
  ,occasion_name_3
  ,occupation
  ,race_code
  ,religion_code
  ,remarks
  ,salutation
  ,spare_attributes
  ,time_based_attributes
  ,status
  ,timestamp(last_update_date) as last_update_date
  ,last_update_by
  ,timestamp(last_approve_date) as last_approve_date
  ,last_approve_by
  ,country_issued_1
  ,country_issued_2
  ,record_no
  ,business_id
  ,marital_status
  ,timestamp(wedding_aniversary) as wedding_aniversary
  ,income_band_id
  ,batch_number
  ,cycle
  ,timestamp(retention_date) as retention_date
  ,id_number
  ,timestamp(first_logged_on_time) as first_logged_on_time
  ,client_id_3 as client_id_3_sha256
  ,client_id_4 as client_id_4_sha256
  ,exchange_id
  FROM staging.cls_blp_client) S
ON
  ifnull(T.csn, '?^$@') = ifnull(S.csn, '?^$@')
  WHEN MATCHED THEN UPDATE SET T.application_date = S.application_date, T.birthplace = S.birthplace, T.citizen = S.citizen, T.client_id_1_sha1 = S.client_id_1_sha1, T.client_id_1_sha256 = S.client_id_1_sha256, T.client_id_2_sha1 = S.client_id_2_sha1, T.client_id_2_sha256 = S.client_id_2_sha256, T.client_name_english_hash = S.client_name_english_hash, T.company_name_1 = S.company_name_1, T.company_name_2 = S.company_name_2, T.country_code = S.country_code, T.csn = S.csn, T.designation = S.designation, T.dob = S.dob, T.external_reference_no_hash = S.external_reference_no_hash, T.first_name_english_hash = S.first_name_english_hash, T.gender_code = S.gender_code, T.id_type_1 = S.id_type_1, T.id_type_2 = S.id_type_2, T.last_name_english_hash = S.last_name_english_hash, T.middle_name_english_hash = S.middle_name_english_hash, T.name_suffix_english_hash = S.name_suffix_english_hash, T.occasion_date_1 = S.occasion_date_1, T.occasion_date_2 = S.occasion_date_2, T.occasion_date_3 = S.occasion_date_3, T.occasion_name_1 = S.occasion_name_1, T.occasion_name_2 = S.occasion_name_2, T.occasion_name_3 = S.occasion_name_3, T.occupation = S.occupation, T.race_code = S.race_code, T.religion_code = S.religion_code, T.remarks = S.remarks, T.salutation = S.salutation, T.spare_attributes = S.spare_attributes, T.time_based_attributes = S.time_based_attributes, T.status = S.status, T.last_update_date = S.last_update_date, T.last_update_by = S.last_update_by, T.last_approve_date = S.last_approve_date, T.last_approve_by = S.last_approve_by, T.country_issued_1 = S.country_issued_1, T.country_issued_2 = S.country_issued_2, T.record_no = S.record_no, T.business_id = S.business_id, T.marital_status = S.marital_status, T.wedding_aniversary = S.wedding_aniversary, T.income_band_id = S.income_band_id, T.batch_number = S.batch_number, T.cycle = S.cycle, T.retention_date = S.retention_date, T.id_number = S.id_number, T.first_logged_on_time = S.first_logged_on_time, T.client_id_3_sha256 = S.client_id_3_sha256, T.client_id_4_sha256 = S.client_id_4_sha256, T.exchange_id = S.exchange_id
  WHEN NOT MATCHED
  THEN
INSERT
  (application_date, birthplace, citizen, client_id_1_sha1, client_id_1_sha256, client_id_2_sha1, client_id_2_sha256, client_name_english_hash, company_name_1, company_name_2, country_code, csn, designation, dob, external_reference_no_hash, first_name_english_hash, gender_code, id_type_1, id_type_2, last_name_english_hash, middle_name_english_hash, name_suffix_english_hash, occasion_date_1, occasion_date_2, occasion_date_3, occasion_name_1, occasion_name_2, occasion_name_3, occupation, race_code, religion_code, remarks, salutation, spare_attributes, time_based_attributes, status, last_update_date, last_update_by, last_approve_date, last_approve_by, country_issued_1, country_issued_2, record_no, business_id, marital_status, wedding_aniversary, income_band_id, batch_number, cycle, retention_date, id_number, first_logged_on_time, client_id_3_sha256, client_id_4_sha256, exchange_id)
VALUES (S.application_date,S.birthplace,S.citizen,S.client_id_1_sha1,S.client_id_1_sha256,S.client_id_2_sha1,S.client_id_2_sha256,S.client_name_english_hash,S.company_name_1,S.company_name_2,S.country_code,S.csn,S.designation,S.dob,S.external_reference_no_hash,S.first_name_english_hash,S.gender_code,S.id_type_1,S.id_type_2,S.last_name_english_hash,S.middle_name_english_hash,S.name_suffix_english_hash,S.occasion_date_1,S.occasion_date_2,S.occasion_date_3,S.occasion_name_1,S.occasion_name_2,S.occasion_name_3,S.occupation,S.race_code,S.religion_code,S.remarks,S.salutation,S.spare_attributes,S.time_based_attributes,S.status,S.last_update_date,S.last_update_by,S.last_approve_date,S.last_approve_by,S.country_issued_1,S.country_issued_2,S.record_no,S.business_id,S.marital_status,S.wedding_aniversary,S.income_band_id,S.batch_number,S.cycle,S.retention_date,S.id_number,S.first_logged_on_time,S.client_id_3_sha256,S.client_id_4_sha256,S.exchange_id)
