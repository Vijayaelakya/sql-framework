begin
	delete from link.link_cls_card_bin where true;
    
    insert into link.link_cls_card_bin select distinct
	 record_no
	,business_id
	,trim(upper(card_bin)) as card_bin
	,trim(upper(description)) as description
	,last_card_seq_no
	,trim(upper(status)) as status
	,date(timestamp(last_update_date)) as last_update_date
	,trim(lower(last_update_by)) as last_update_by 
	,date(timestamp(last_approve_date)) as last_approve_date
	,trim(lower(last_approve_by)) as last_approve_by 
	,virtual_card_type_ind
	from
	staging.cls_blp_card_bin;
end
