BEGIN

DELETE FROM link.link_cls_transaction_code
WHERE ifnull(TRANSACTION_CODE, '?^$@') in (Select ifnull(TRANSACTION_CODE, '?^$@') from staging.cls_blp_transaction_code);

INSERT INTO link.link_cls_transaction_code
SELECT DISTINCT transaction_code,
	    club_code,
	    description_english,
	    description_language2,
	    status,
	    date(timestamp(last_approve_date)) as last_approve_date,
	    last_approve_by,
	    date(timestamp(last_update_date)) as last_update_date,
	    last_update_by,
	    transaction_type,
	    record_no,
	    business_id,
	    pre_award,
	    exclude_statement,
	    exclude_stmt_ind,
	    date(timestamp(retention_date)) as retention_date,
	    special_point_ind
	from staging.cls_blp_transaction_code;

END;
