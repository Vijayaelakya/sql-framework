MERGE
  link.link_cls_product_account T
USING
  (select distinct 
	stage.product_account_no,
	stage.csn,
	stage.product_account_type,
	date(timestamp(stage.termination_date)) as termination_date,
	stage.status,
	stage.last_update_by as last_upd_by,
	timestamp(stage.last_update_date) as last_upd_date
	from staging.cls_blp_product_account stage,
	(
	select product_account_no, csn, max(last_update_date) max_last_upd_date
	from staging.cls_blp_product_account
	group by product_account_no, csn
	) x
	where stage.product_account_no = x.product_account_no
	and stage.csn = x.csn
	and stage.last_update_date = x.max_last_upd_date) S
ON
ifnull(T.product_account_no, '?^$@') = ifnull(S.product_account_no, '?^$@')
	and ifnull(T.csn, '?^$@') = ifnull(S.csn, '?^$@')
WHEN MATCHED THEN UPDATE SET 
T.product_account_no = S.product_account_no,
T.csn = S.csn,
T.product_account_type = S.product_account_type,
T.termination_date = S.termination_date,
T.status = S.status,
T.last_upd_by = S.last_upd_by,
T.last_upd_date = S.last_upd_date
  WHEN NOT MATCHED
  THEN
INSERT
(product_account_no,csn,product_account_type,termination_date,status,last_upd_by,last_upd_date)
VALUES (S.product_account_no,S.csn,S.product_account_type,S.termination_date,S.status,S.last_upd_by,S.last_upd_date)
