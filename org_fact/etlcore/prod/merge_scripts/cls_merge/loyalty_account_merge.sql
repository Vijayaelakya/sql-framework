MERGE
  link.link_cls_loyalty_account T
USING
  (
  SELECT
    DISTINCT t1.loyalty_account_no,
    t1.club_code,
    t1.csn,
    t1.loyalty_account_first_name,
    t1.loyalty_account_middle_name,
    t1.loyalty_account_last_name,
    t1.loyalty_account_class,
    t1.loyalty_account_group,
    t1.loyalty_account_segment,
    t1.loyalty_account_status,
    t1.status,
    t1.last_update_by,
    TIMESTAMP(t1.last_update_date) AS last_upd_date
  FROM
    staging.cls_blp_loyalty_account t1,
    (
    SELECT
      loyalty_account_no,
      club_code,
      MAX(last_update_date) max_last_upd_date
    FROM
      staging.cls_blp_loyalty_account
    GROUP BY
      loyalty_account_no,
      club_code ) X
  WHERE
    t1.loyalty_account_no = X.loyalty_account_no
    AND t1.club_code = X.club_code
    AND t1.last_update_date = X.max_last_upd_date) S
ON
  ifnull(T.loyalty_account_no, '?^$@') = ifnull(S.loyalty_account_no, '?^$@')
  AND ifnull(T.club_code, '?^$@') = ifnull(S.club_code, '?^$@')
  WHEN MATCHED THEN UPDATE SET T.loyalty_account_no = S.loyalty_account_no, T.club_code = S.club_code, T.csn = S.csn, T.loyalty_account_first_name = S.loyalty_account_first_name, T.loyalty_account_middle_name = S.loyalty_account_middle_name, T.loyalty_account_last_name = S.loyalty_account_last_name, T.loyalty_account_class = S.loyalty_account_class, T.loyalty_account_group = S.loyalty_account_group, T.loyalty_account_segment = S.loyalty_account_segment, T.loyalty_account_status = S.loyalty_account_status, T.status = S.status, T.last_upd_by = S.last_update_by, T.last_upd_date = S.last_upd_date
  WHEN NOT MATCHED
  THEN
INSERT
  (loyalty_account_no,
    club_code,
    csn,
    loyalty_account_first_name,
    loyalty_account_middle_name,
    loyalty_account_last_name,
    loyalty_account_class,
    loyalty_account_group,
    loyalty_account_segment,
    loyalty_account_status,
    status,
    last_upd_by,
    last_upd_date)
VALUES (S.loyalty_account_no,S.club_code,S.csn,S.loyalty_account_first_name,S.loyalty_account_middle_name,S.loyalty_account_last_name,S.loyalty_account_class,S.loyalty_account_group,S.loyalty_account_segment,S.loyalty_account_status,S.status,S.last_update_by,S.last_upd_date)
