MERGE
  link.link_cls_loyalty_account_balance T
USING
  (select distinct 
	t1.loyalty_account_no,
	t1.club_code,
	t1.pool_id,
	t1.sub_pool_id,
	t1.seq_no,
	cast(t1.balance as numeric) as balance,
	date(timestamp(t1.start_date)) as start_date,
	date(timestamp(t1.expiry_date)) as expiry_date,
	t1.status,
	t1.last_update_by as last_upd_by,
	timestamp(t1.last_update_date) as last_upd_date
	from `staging.cls_blp_loyalty_account_balance` t1,
	(
	select loyalty_account_no, club_code, pool_id, sub_pool_id, seq_no, max(last_update_date) max_last_upd_date
	from `staging.cls_blp_loyalty_account_balance` 
	group by loyalty_account_no, club_code, pool_id, sub_pool_id, seq_no
	) x
	where t1.loyalty_account_no = x.loyalty_account_no
	and t1.club_code = x.club_code
	and t1.pool_id = x.pool_id
	and t1.sub_pool_id = x.sub_pool_id
	and t1.seq_no = x.seq_no
	and t1.last_update_date = x.max_last_upd_date) S
ON
ifnull(T.loyalty_account_no, '?^$@') = ifnull(S.loyalty_account_no, '?^$@')
	and ifnull(T.club_code, '?^$@') = ifnull(S.club_code, '?^$@')
	and ifnull(T.pool_id, '?^$@') = ifnull(S.pool_id, '?^$@')
	and ifnull(T.sub_pool_id, '?^$@') = ifnull(S.sub_pool_id, '?^$@')
	and ifnull(T.seq_no, -999) = ifnull(S.seq_no, -999)
  WHEN MATCHED THEN UPDATE SET 
T.loyalty_account_no = S.loyalty_account_no,
T.club_code = S.club_code,
T.pool_id = S.pool_id,
T.sub_pool_id = S.sub_pool_id,
T.seq_no = S.seq_no,
T.balance = S.balance,
T.start_date = S.start_date,
T.expiry_date = S.expiry_date,
T.status = S.status,
T.last_upd_by = S.last_upd_by,
T.last_upd_date = S.last_upd_date
  WHEN NOT MATCHED
  THEN
INSERT
(loyalty_account_no,club_code,pool_id,sub_pool_id,seq_no,balance,start_date,expiry_date,status,last_upd_by,last_upd_date)
VALUES (S.loyalty_account_no,S.club_code,S.pool_id,S.sub_pool_id,S.seq_no,S.balance,S.start_date,S.expiry_date,S.status,S.last_upd_by,S.last_upd_date)
