MERGE
  link.link_cls_proxy_card_mapping T
USING
  (select 
		proxy_no,
		card_no,
		status,
		timestamp(last_update_date) as last_update_date,
		last_update_by,
		business_id
	from staging.cls_blp_proxy_card_mapping) S
ON
T.proxy_no = S.proxy_no
			and T.card_no = S.card_no
			and T.status = S.status
			and T.business_id = S.business_id
WHEN MATCHED THEN UPDATE SET 
T.proxy_no = S.proxy_no,
T.card_no = S.card_no,
T.status = S.status,
T.last_update_date = S.last_update_date,
T.last_update_by = S.last_update_by,
T.business_id = S.business_id
  WHEN NOT MATCHED
  THEN
INSERT
(proxy_no,card_no,status,last_update_date,last_update_by,business_id)
VALUES (S.proxy_no,S.card_no,S.status,S.last_update_date,S.last_update_by,S.business_id)
