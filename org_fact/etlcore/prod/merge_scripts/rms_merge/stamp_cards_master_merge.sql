MERGE
  rms.rms_stamp_cards_master T
USING
  (SELECT DISTINCT
     stamp_card_id,
      title as title,
      sub_title as sub_title,
     status_code,
     merchant_id,
      tnc as tnc,
     start_date,
     creation_time,
     last_updated_at,
      description as description,
     provided_inventory,
     total_inventory,
     end_date,
     max_visits_per_hour,
      text_on_stamp as text_on_stamp,
      small_img_url as small_img_url,
      big_img_url as big_img_url,
      cast(is_header as STRING) as is_header,
      header_title as header_title,
      header_subtitle as header_subtitle,
      header_img_url as header_img_url,
     header_publish_date,
     header_expiry_date,
      goal_asset_url as goal_asset_url,
      milestone_text as milestone_text,
     total_steps,
     max_redemption_count,
      marketing_event_name as marketing_event_name,
     type,
      location as location
  FROM staging.rms_rmsdb_stamp_cards_master) S
ON
T.stamp_card_id = S.stamp_card_id
WHEN MATCHED THEN UPDATE SET 
T.stamp_card_id = S.stamp_card_id,
T.title = S.title,
T.sub_title = S.sub_title,
T.status_code = S.status_code,
T.merchant_id = S.merchant_id,
T.tnc = S.tnc,
T.start_date = S.start_date,
T.creation_time = S.creation_time,
T.last_updated_at = S.last_updated_at,
T.description = S.description,
T.provided_inventory = S.provided_inventory,
T.total_inventory = S.total_inventory,
T.end_date = S.end_date,
T.max_visits_per_hour = S.max_visits_per_hour,
T.text_on_stamp = S.text_on_stamp,
T.small_img_url = S.small_img_url,
T.big_img_url = S.big_img_url,
T.is_header = S.is_header,
T.header_title = S.header_title,
T.header_subtitle = S.header_subtitle,
T.header_img_url = S.header_img_url,
T.header_publish_date = S.header_publish_date,
T.header_expiry_date = S.header_expiry_date,
T.goal_asset_url = S.goal_asset_url,
T.milestone_text = S.milestone_text,
T.total_steps = S.total_steps,
T.max_redemption_count = S.max_redemption_count,
T.marketing_event_name = S.marketing_event_name,
T.type = S.type,
T.location = S.location
  WHEN NOT MATCHED
  THEN
INSERT
(stamp_card_id,title,sub_title,status_code,merchant_id,tnc,start_date,creation_time,last_updated_at,description,provided_inventory,total_inventory,end_date,max_visits_per_hour,text_on_stamp,small_img_url,big_img_url,is_header,header_title,header_subtitle,header_img_url,header_publish_date,header_expiry_date,goal_asset_url,milestone_text,total_steps,max_redemption_count,marketing_event_name,type,location)
VALUES (S.stamp_card_id,S.title,S.sub_title,S.status_code,S.merchant_id,S.tnc,S.start_date,S.creation_time,S.last_updated_at,S.description,S.provided_inventory,S.total_inventory,S.end_date,S.max_visits_per_hour,S.text_on_stamp,S.small_img_url,S.big_img_url,S.is_header,S.header_title,S.header_subtitle,S.header_img_url,S.header_publish_date,S.header_expiry_date,S.goal_asset_url,S.milestone_text,S.total_steps,S.max_redemption_count,S.marketing_event_name,S.type,S.location)
