MERGE
  rms.rms_rewards_deals T
USING
  (SELECT DISTINCT
     id,
     user_id as user_id,
     deal_id,
     date_added,
     status,
     updated_date,
     coupon_code_id
  FROM staging.rms_rmsdb_rewards_deals) S
ON
T.id = S.id
WHEN MATCHED THEN UPDATE SET 
T.id = S.id,
T.user_id = S.user_id,
T.deal_id = S.deal_id,
T.date_added = S.date_added,
T.status = S.status,
T.updated_date = S.updated_date,
T.coupon_code_id = S.coupon_code_id
  WHEN NOT MATCHED
  THEN
INSERT
(id,user_id,deal_id,date_added,status,updated_date,coupon_code_id)
VALUES (S.id,S.user_id,S.deal_id,S.date_added,S.status,S.updated_date,S.coupon_code_id)
