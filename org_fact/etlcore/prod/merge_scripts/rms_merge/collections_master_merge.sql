MERGE
  rms.rms_collections_master T
USING
  (SELECT DISTINCT
   collection_id,
   title as title,
   sub_title as sub_title,
   publish_date,
   collection_type,
   creation_time,
   updated_at,
   status,
    small_img_url as small_img_url,
    is_header as is_header,
    header_title as header_title,
    header_subtitle as header_subtitle,
    header_img_url as header_img_url,
    header_small_img_url as header_small_img_url,
    header_publish_date,
    header_expiry_date,
    banner_img_url as banner_img_url,
    filter_icon_url as filter_icon_url,
    cast(is_filtering_allowed AS STRING) as is_filtering_allowed
FROM staging.rms_rmsdb_collections_master) S
ON
T.collection_id = S.collection_id
WHEN MATCHED THEN UPDATE SET 
T.collection_id = S.collection_id,
T.title = S.title,
T.sub_title = S.sub_title,
T.publish_date = S.publish_date,
T.collection_type = S.collection_type,
T.creation_time = S.creation_time,
T.updated_at = S.updated_at,
T.status = S.status,
T.small_img_url = S.small_img_url,
T.is_header = cast(S.is_header as STRING),
T.header_title = S.header_title,
T.header_subtitle = S.header_subtitle,
T.header_img_url = S.header_img_url,
T.header_small_img_url = S.header_small_img_url,
T.header_publish_date = S.header_publish_date,
T.header_expiry_date = S.header_expiry_date,
T.banner_img_url = S.banner_img_url,
T.filter_icon_url = S.filter_icon_url,
T.is_filtering_allowed = S.is_filtering_allowed
  WHEN NOT MATCHED
  THEN
INSERT
(collection_id,title,sub_title,publish_date,collection_type,creation_time,updated_at,status,small_img_url,is_header,header_title,header_subtitle,header_img_url,header_small_img_url,header_publish_date,header_expiry_date,banner_img_url,filter_icon_url,is_filtering_allowed)
VALUES (S.collection_id,S.title,S.sub_title,S.publish_date,S.collection_type,S.creation_time,S.updated_at,S.status,S.small_img_url,cast(S.is_header as STRING),S.header_title,S.header_subtitle,S.header_img_url,S.header_small_img_url,S.header_publish_date,S.header_expiry_date,S.banner_img_url,S.filter_icon_url,S.is_filtering_allowed)
