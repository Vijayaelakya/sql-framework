MERGE
  rms.rms_user_table T
USING
  (SELECT DISTINCT
		user_id,
		creation_date,
		aid,
		updated_date
	FROM staging.rms_rmsdb_user_table) S
ON
T.user_id = S.user_id
WHEN MATCHED THEN UPDATE SET 
T.user_id = S.user_id,
T.creation_date = S.creation_date,
T.aid = S.aid,
T.updated_date = S.updated_date
  WHEN NOT MATCHED
  THEN
INSERT
(user_id,creation_date,aid,updated_date)
VALUES (S.user_id,S.creation_date,S.aid,S.updated_date)
