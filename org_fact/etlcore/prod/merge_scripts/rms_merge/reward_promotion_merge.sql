MERGE
  rms.rms_reward_promotion T
USING
  (SELECT DISTINCT
     id,
     user_id as user_id,
     promotion_id,
     date_added,
     status,
     updated_date
  FROM staging.rms_rmsdb_reward_promotion) S
ON
T.id = S.id
WHEN MATCHED THEN UPDATE SET 
T.id = S.id,
T.user_id = S.user_id,
T.promotion_id = S.promotion_id,
T.date_added = S.date_added,
T.status = S.status,
T.updated_date = S.updated_date
  WHEN NOT MATCHED
  THEN
INSERT
(id,user_id,promotion_id,date_added,status,updated_date)
VALUES (S.id,S.user_id,S.promotion_id,S.date_added,S.status,S.updated_date)
