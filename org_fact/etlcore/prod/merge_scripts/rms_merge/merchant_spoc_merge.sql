MERGE
  rms.rms_merchant_spoc T
USING
  (SELECT DISTINCT
       spoc_id,
        first_name as first_name,
        last_name as last_name,
       store_id,
        mobile_number as mobile_number,
        email_addr as email_addr,
       created_date,
       updated_date    
    FROM staging.rms_rmsdb_merchant_spoc) S
ON
T.spoc_id = S.spoc_id
WHEN MATCHED THEN UPDATE SET 
T.spoc_id = S.spoc_id,
T.first_name = S.first_name,
T.last_name = S.last_name,
T.store_id = S.store_id,
T.mobile_number = S.mobile_number,
T.email_addr = S.email_addr,
T.created_date = S.created_date,
T.updated_date = S.updated_date
  WHEN NOT MATCHED
  THEN
INSERT
(spoc_id,first_name,last_name,store_id,mobile_number,email_addr,created_date,updated_date)
VALUES (S.spoc_id,S.first_name,S.last_name,S.store_id,S.mobile_number,S.email_addr,S.created_date,S.updated_date)
