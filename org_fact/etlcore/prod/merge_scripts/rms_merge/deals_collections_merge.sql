begin
delete from `rms.rms_deals_collections` where true;

INSERT INTO rms.rms_deals_collections 
SELECT * FROM (
	SELECT DISTINCT
	   id,
	   deal_id ,
	   collection_id,
	   rank
	FROM staging.rms_rmsdb_deals_collections
);
end;
