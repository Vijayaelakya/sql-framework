MERGE rms.rms_promotions_master T
USING (
  SELECT DISTINCT promotion_id
    ,title
    ,sub_title
    ,merchant_id
    ,tnc tnc
    ,start_date
    ,creation_time
    ,last_updated_at
    ,description
    ,end_date
    ,small_img_url
    ,big_img_url
    ,is_header
    ,header_title
    ,header_subtitle
    ,header_img_url
    ,header_publish_date
    ,header_expiry_date
    ,promotion_type
    ,promotion_url
    ,status
    ,device_type
    ,permalink
    ,popularity_score
    ,is_limited_store
    ,meta_title
    ,meta_description
    ,web_description
  FROM staging.rms_rmsdb_promotions_master
  ) S
  ON T.promotion_id = S.promotion_id
WHEN MATCHED
  THEN
    UPDATE
    SET T.promotion_id = S.promotion_id
      ,T.title = S.title
      ,T.sub_title = S.sub_title
      ,T.merchant_id = S.merchant_id
      ,T.tnc = S.tnc
      ,T.start_date = S.start_date
      ,T.creation_time = S.creation_time
      ,T.last_updated_at = S.last_updated_at
      ,T.description = S.description
      ,T.end_date = S.end_date
      ,T.small_img_url = S.small_img_url
      ,T.big_img_url = S.big_img_url
      ,T.is_header = CAST(S.is_header AS STRING)
      ,T.header_title = S.header_title
      ,T.header_subtitle = S.header_subtitle
      ,T.header_img_url = S.header_img_url
      ,T.header_publish_date = S.header_publish_date
      ,T.header_expiry_date = S.header_expiry_date
      ,T.promotion_type = S.promotion_type
      ,T.promotion_url = S.promotion_url
      ,T.status = S.status
      ,T.device_type = S.device_type
      ,T.permalink = S.permalink
      ,T.popularity_score = S.popularity_score
      ,T.is_limited_store = S.is_limited_store
      ,T.meta_title = S.meta_title
      ,T.meta_description = S.meta_description
      ,T.web_description = S.web_description
WHEN NOT MATCHED
  THEN
    INSERT (
      promotion_id
      ,title
      ,sub_title
      ,merchant_id
      ,tnc
      ,start_date
      ,creation_time
      ,last_updated_at
      ,description
      ,end_date
      ,small_img_url
      ,big_img_url
      ,is_header
      ,header_title
      ,header_subtitle
      ,header_img_url
      ,header_publish_date
      ,header_expiry_date
      ,promotion_type
      ,promotion_url
      ,status
      ,device_type
      ,permalink
      ,popularity_score
      ,is_limited_store
      ,meta_title
      ,meta_description
      ,web_description
      )
    VALUES (
      S.promotion_id
      ,S.title
      ,S.sub_title
      ,S.merchant_id
      ,S.tnc
      ,S.start_date
      ,S.creation_time
      ,S.last_updated_at
      ,S.description
      ,S.end_date
      ,S.small_img_url
      ,S.big_img_url
      ,CAST(S.is_header AS STRING)
      ,S.header_title
      ,S.header_subtitle
      ,S.header_img_url
      ,S.header_publish_date
      ,S.header_expiry_date
      ,S.promotion_type
      ,S.promotion_url
      ,S.status
      ,S.device_type
      ,S.permalink
      ,S.popularity_score
      ,S.is_limited_store
      ,S.meta_title
      ,S.meta_description
      ,S.web_description
      )
    ;

