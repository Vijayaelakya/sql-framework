MERGE
  rms.rms_merchant_stores T
USING
  (SELECT DISTINCT
     store_id,
     merchant_id,
      geoX as geoX, 
      geoY as geoY,
      store_name as store_name,
      address_line1 as address_line1,
      address_line2 as address_line2,
      postal_code as postal_code,
     sms_opt_in,
     email_opt_in,
     store_type,
      unit_number as unit_number,
      level as level,
      pin as pin,
     pin_sent_date,
     pin_delivery_schedule,
     created_date,
     updated_date,
      echoss_store_id as echoss_store_id,
      cls_operator_id as cls_operator_id,
     cls_merchant_id,
     cls_terminal_id,
     cls_pin,
      qr_code as qr_code,
      cast(status as STRING) as status,
     is_milestone
  FROM staging.rms_rmsdb_merchant_stores) S
ON
T.store_id = S.store_id
WHEN MATCHED THEN UPDATE SET 
T.store_id = S.store_id,
T.merchant_id = S.merchant_id,
T.geox = S.geox,
T.geoy = S.geoy,
T.store_name = S.store_name,
T.address_line1 = S.address_line1,
T.address_line2 = S.address_line2,
T.postal_code = S.postal_code,
T.sms_opt_in = S.sms_opt_in,
T.email_opt_in = S.email_opt_in,
T.store_type = S.store_type,
T.unit_number = S.unit_number,
T.level = S.level,
T.pin = S.pin,
T.pin_sent_date = S.pin_sent_date,
T.pin_delivery_schedule = S.pin_delivery_schedule,
T.created_date = S.created_date,
T.updated_date = S.updated_date,
T.echoss_store_id = S.echoss_store_id,
T.cls_operator_id = S.cls_operator_id,
T.cls_merchant_id = S.cls_merchant_id,
T.cls_terminal_id = S.cls_terminal_id,
T.cls_pin = S.cls_pin,
T.qr_code = S.qr_code,
T.status = S.status,
T.is_milestone = S.is_milestone
  WHEN NOT MATCHED
  THEN
INSERT
(store_id,merchant_id,geox,geoy,store_name,address_line1,address_line2,postal_code,sms_opt_in,email_opt_in,store_type,unit_number,level,pin,pin_sent_date,pin_delivery_schedule,created_date,updated_date,echoss_store_id,cls_operator_id,cls_merchant_id,cls_terminal_id,cls_pin,qr_code,status,is_milestone)
VALUES (S.store_id,S.merchant_id,S.geox,S.geoy,S.store_name,S.address_line1,S.address_line2,S.postal_code,S.sms_opt_in,S.email_opt_in,S.store_type,S.unit_number,S.level,S.pin,S.pin_sent_date,S.pin_delivery_schedule,S.created_date,S.updated_date,S.echoss_store_id,S.cls_operator_id,S.cls_merchant_id,S.cls_terminal_id,S.cls_pin,S.qr_code,S.status,S.is_milestone)
