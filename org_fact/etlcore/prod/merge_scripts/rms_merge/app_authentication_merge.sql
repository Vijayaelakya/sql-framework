MERGE
  rms.rms_app_authentication T
USING
  (SELECT DISTINCT
	   id,
	   app_id as app_id,
	   app_secret as app_secret,
	   endpoint_name as endpoint_name,
	   source_ip as source_ip,
	   created_date,
	   updated_date
	FROM staging.rms_rmsdb_app_authentication) S
ON
T.id = S.id
WHEN MATCHED THEN UPDATE SET 
T.id = S.id,
T.app_id = S.app_id,
T.app_secret = S.app_secret,
T.endpoint_name = S.endpoint_name,
T.source_ip = S.source_ip,
T.created_date = S.created_date,
T.updated_date = S.updated_date
  WHEN NOT MATCHED
  THEN
INSERT
(id,app_id,app_secret,endpoint_name,source_ip,created_date,updated_date)
VALUES (S.id,S.app_id,S.app_secret,S.endpoint_name,S.source_ip,S.created_date,S.updated_date)
