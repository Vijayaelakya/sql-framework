MERGE
  rms.rms_merchant_master T
USING
  (SELECT DISTINCT
     merchant_id,
      merchant_name as merchant_name,
      subtitle as subtitle,
     status,
     publish_date,
      our_story as our_story,
     creation_time,
     last_updated_at,
      small_img_url as small_img_url,
      big_img_url as big_img_url,
      cast(is_header as STRING) as is_header,
      header_title as header_title,
      header_subtitle as header_subtitle,
      header_img_url as header_img_url,
     header_publish_date,
     header_expiry_date,
     issuance_rate_linkpoint,
     issuance_rate_dollar_value,
      description as description,
      web_url as web_url,
      tnc as tnc,
      echoss_merchant_id as echoss_merchant_id
  FROM staging.rms_rmsdb_merchant_master) S
ON
T.merchant_id = S.merchant_id
WHEN MATCHED THEN UPDATE SET 
T.merchant_id = S.merchant_id,
T.merchant_name = S.merchant_name,
T.subtitle = S.subtitle,
T.status = S.status,
T.publish_date = S.publish_date,
T.our_story = S.our_story,
T.creation_time = (S.creation_time),
T.last_updated_at = S.last_updated_at,
T.small_img_url = S.small_img_url,
T.big_img_url = S.big_img_url,
T.is_header = S.is_header,
T.header_title = S.header_title,
T.header_subtitle = S.header_subtitle,
T.header_img_url = S.header_img_url,
T.header_publish_date = S.header_publish_date,
T.header_expiry_date = S.header_expiry_date,
T.issuance_rate_linkpoint = safe_cast(S.issuance_rate_linkpoint as int64),
T.issuance_rate_dollar_value = S.issuance_rate_dollar_value,
T.description = S.description,
T.web_url = S.web_url,
T.tnc = S.tnc,
T.echoss_merchant_id = S.echoss_merchant_id
  WHEN NOT MATCHED
  THEN
INSERT
(merchant_id,merchant_name,subtitle,status,publish_date,our_story,creation_time,last_updated_at,small_img_url,big_img_url,is_header,header_title,header_subtitle,header_img_url,header_publish_date,header_expiry_date,issuance_rate_linkpoint,issuance_rate_dollar_value,description,web_url,tnc,echoss_merchant_id)
VALUES (S.merchant_id,S.merchant_name,S.subtitle,S.status,S.publish_date,S.our_story,S.creation_time,S.last_updated_at,S.small_img_url,S.big_img_url,S.is_header,S.header_title,S.header_subtitle,S.header_img_url,S.header_publish_date,S.header_expiry_date,safe_cast(S.issuance_rate_linkpoint as int64),S.issuance_rate_dollar_value,S.description,S.web_url,S.tnc,S.echoss_merchant_id)
