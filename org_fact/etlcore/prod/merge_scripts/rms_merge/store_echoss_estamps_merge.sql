MERGE
  rms.rms_store_echoss_estamps T
USING
  (SELECT DISTINCT
	   id,
	   merchant_store_id,
	    echoss_estamp_id as echoss_estamp_id,
	    echoss_estamp_serial_no as echoss_estamp_serial_no,
	   created_date,
	   updated_date
	FROM staging.rms_rmsdb_store_echoss_estamps) S
ON
T.id = S.id
WHEN MATCHED THEN UPDATE SET 
T.id = S.id,
T.merchant_store_id = S.merchant_store_id,
T.echoss_estamp_id = S.echoss_estamp_id,
T.echoss_estamp_serial_no = S.echoss_estamp_serial_no,
T.created_date = S.created_date,
T.updated_date = S.updated_date
  WHEN NOT MATCHED
  THEN
INSERT
(id,merchant_store_id,echoss_estamp_id,echoss_estamp_serial_no,created_date,updated_date)
VALUES (S.id,S.merchant_store_id,S.echoss_estamp_id,S.echoss_estamp_serial_no,S.created_date,S.updated_date)
