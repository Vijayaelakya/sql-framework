MERGE
  rms.rms_deal_coupon_codes T
USING
  (SELECT DISTINCT
     coupon_code_id,
     deal_id,
     coupon_code as coupon_code,
     is_used,
     created_date,
     updated_date
  FROM staging.rms_rmsdb_deal_coupon_codes) S
ON
T.coupon_code_id = S.coupon_code_id
WHEN MATCHED THEN UPDATE SET 
T.coupon_code_id = S.coupon_code_id,
T.deal_id = S.deal_id,
T.coupon_code = S.coupon_code,
T.is_used = S.is_used,
T.created_date = S.created_date,
T.updated_date = S.updated_date
  WHEN NOT MATCHED
  THEN
INSERT
(coupon_code_id,deal_id,coupon_code,is_used,created_date,updated_date)
VALUES (S.coupon_code_id,S.deal_id,S.coupon_code,S.is_used,S.created_date,S.updated_date)
