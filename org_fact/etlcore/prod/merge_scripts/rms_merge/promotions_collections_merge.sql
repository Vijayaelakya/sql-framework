begin

DELETE FROM rms.rms_promotions_collections where true;

INSERT INTO rms.rms_promotions_collections 
SELECT * FROM (
	SELECT DISTINCT
	   promotion_id,
	   collection_id
	FROM staging.rms_rmsdb_promotions_collections
);

end
