MERGE
  rms.rms_rewards_stamps T
USING
  (SELECT DISTINCT
     id,
     user_id as user_id,
     stamp_card_id,
     date_added,
     total_stamps_on_card,
     stamp_status,
     updated_date  
  FROM staging.rms_rmsdb_rewards_stamps) S
ON
T.id = S.id
WHEN MATCHED THEN UPDATE SET 
T.id = S.id,
T.user_id = S.user_id,
T.stamp_card_id = S.stamp_card_id,
T.date_added = S.date_added,
T.total_stamps_on_card = S.total_stamps_on_card,
T.stamp_status = S.stamp_status,
T.updated_date = S.updated_date
  WHEN NOT MATCHED
  THEN
INSERT
(id,user_id,stamp_card_id,date_added,total_stamps_on_card,stamp_status,updated_date)
VALUES (S.id,S.user_id,S.stamp_card_id,S.date_added,S.total_stamps_on_card,S.stamp_status,S.updated_date)
