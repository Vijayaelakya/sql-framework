MERGE
  rms.rms_blocked_stamps_rewards T
USING
  (SELECT DISTINCT
	   blocked_stamps_id,
	   user_id,
	   blocked_till_date_time,
	   unsuccessful_attempt_count,
	   last_pin_attempt_date_time,
	   reward_stamps_id,
	   stamp_card_id
    FROM staging.rms_rmsdb_blocked_stamps_rewards) S
ON
T.blocked_stamps_id = S.blocked_stamps_id
WHEN MATCHED THEN UPDATE SET 
T.blocked_stamps_id = S.blocked_stamps_id,
T.user_id = S.user_id,
T.blocked_till_date_time = S.blocked_till_date_time,
T.unsuccessful_attempt_count = S.unsuccessful_attempt_count,
T.last_pin_attempt_date_time = S.last_pin_attempt_date_time,
T.reward_stamps_id = S.reward_stamps_id,
T.stamp_card_id = S.stamp_card_id
  WHEN NOT MATCHED
  THEN
INSERT
(blocked_stamps_id,user_id,blocked_till_date_time,unsuccessful_attempt_count,last_pin_attempt_date_time,reward_stamps_id,stamp_card_id)
VALUES (S.blocked_stamps_id,S.user_id,S.blocked_till_date_time,S.unsuccessful_attempt_count,S.last_pin_attempt_date_time,S.reward_stamps_id,S.stamp_card_id)
