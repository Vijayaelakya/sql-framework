MERGE
  rms.rms_user_deal_records T
USING
  (SELECT DISTINCT
	    user_id as user_id,
	   deals_record_id,
	   deal_id,
	   deal_time,
	    geoX as geoX,
	    geoY as geoY,
	    ip_addr as ip_addr,
	   merchant_id,
	   from_state,
	   to_state,
	   merchant_store_id,
	   updated_date,
	    source as source,
	   success_status,
	    echoss_metadata as echoss_metadata
	FROM staging.rms_rmsdb_user_deal_records) S
ON
T.deals_record_id = S.deals_record_id
WHEN MATCHED THEN UPDATE SET 
T.user_id = S.user_id,
T.deals_record_id = S.deals_record_id,
T.deal_id = S.deal_id,
T.deal_time = S.deal_time,
T.geox = S.geox,
T.geoy = S.geoy,
T.ip_addr = S.ip_addr,
T.merchant_id = S.merchant_id,
T.from_state = S.from_state,
T.to_state = S.to_state,
T.merchant_store_id = S.merchant_store_id,
T.updated_date = S.updated_date,
T.source = S.source,
T.success_status = S.success_status,
T.echoss_metadata = S.echoss_metadata
  WHEN NOT MATCHED
  THEN
INSERT
(user_id,deals_record_id,deal_id,deal_time,geox,geoy,ip_addr,merchant_id,from_state,to_state,merchant_store_id,updated_date,source,success_status,echoss_metadata)
VALUES (S.user_id,S.deals_record_id,S.deal_id,S.deal_time,S.geox,S.geoy,S.ip_addr,S.merchant_id,S.from_state,S.to_state,S.merchant_store_id,S.updated_date,S.source,S.success_status,S.echoss_metadata)
