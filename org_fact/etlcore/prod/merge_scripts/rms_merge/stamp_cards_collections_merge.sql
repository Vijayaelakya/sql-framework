begin

TRUNCATE TABLE rms.rms_stamp_cards_collections;

INSERT INTO rms.rms_stamp_cards_collections 
SELECT * FROM (
	SELECT DISTINCT
	   stamp_card_id,
	   collection_id,
	   id,
	   cast("rank" as STRING)
	FROM staging.rms_rmsdb_stamp_cards_collections
);

end
