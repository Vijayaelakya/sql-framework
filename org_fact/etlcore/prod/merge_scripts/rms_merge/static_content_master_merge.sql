MERGE
  rms.rms_static_content_master T
USING
  (SELECT DISTINCT
       content_id,
        content_key as content_key,
        content_value as content_value
    FROM staging.rms_rmsdb_static_content_master) S
ON
T.content_id = S.content_id
WHEN MATCHED THEN UPDATE SET 
T.content_id = S.content_id,
T.content_key = S.content_key,
T.content_value = S.content_value
  WHEN NOT MATCHED
  THEN
INSERT
(content_id,content_key,content_value)
VALUES (S.content_id,S.content_key,S.content_value)
