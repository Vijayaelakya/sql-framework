begin

DELETE FROM rms.rms_merchant_collections where true; 

INSERT INTO rms.rms_merchant_collections 
SELECT * FROM (
	SELECT DISTINCT
	   collection_id,
	   merchant_id  
	FROM staging.rms_rmsdb_merchant_collections
);
end
