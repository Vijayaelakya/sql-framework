MERGE
  rms.rms_blocked_merchant_pin T
USING
  (SELECT DISTINCT
	   blocked_pin_id as blocked_pin_id,
	   user_id as user_id,
	   blocked_till_date_time,
	   unsuccessful_attempt_count,
	   last_pin_attempt_date_time,
	   merchant_id,
	   created_date,
	   updated_date
   FROM staging.rms_rmsdb_blocked_merchant_pin) S
ON
T.blocked_pin_id = S.blocked_pin_id
WHEN MATCHED THEN UPDATE SET 
T.blocked_pin_id = S.blocked_pin_id,
T.user_id = S.user_id,
T.blocked_till_date_time = S.blocked_till_date_time,
T.unsuccessful_attempt_count = S.unsuccessful_attempt_count,
T.last_pin_attempt_date_time = S.last_pin_attempt_date_time,
T.merchant_id = S.merchant_id,
T.created_date = S.created_date,
T.updated_date =S.updated_date
  WHEN NOT MATCHED
  THEN
INSERT
(blocked_pin_id,user_id,blocked_till_date_time,unsuccessful_attempt_count,last_pin_attempt_date_time,merchant_id,created_date,updated_date)
VALUES (S.blocked_pin_id,S.user_id,S.blocked_till_date_time,S.unsuccessful_attempt_count,S.last_pin_attempt_date_time,S.merchant_id,S.created_date,S.updated_date)
