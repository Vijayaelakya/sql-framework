MERGE
  rms.rms_blocked_deals_rewards T
USING
  (SELECT DISTINCT
	  blocked_deals_id,
	  user_id,
	  blocked_till_date_time,
	  unsuccessful_attempt_count,
	  last_pin_attempt_date_time,
	  rewards_deals_id,
	  deal_id,
	  created_date,
	  updated_date
    FROM staging.rms_rmsdb_blocked_deals_rewards) S
ON
T.blocked_deals_id = S.blocked_deals_id
WHEN MATCHED THEN UPDATE SET 
T.blocked_deals_id = S.blocked_deals_id,
T.user_id = S.user_id,
T.blocked_till_date_time = S.blocked_till_date_time,
T.unsuccessful_attempt_count = S.unsuccessful_attempt_count,
T.last_pin_attempt_date_time = S.last_pin_attempt_date_time,
T.rewards_deals_id = S.rewards_deals_id,
T.deal_id = S.deal_id,
T.created_date = S.created_date,
T.updated_date = S.updated_date
  WHEN NOT MATCHED
  THEN
INSERT
(blocked_deals_id,user_id,blocked_till_date_time,unsuccessful_attempt_count,last_pin_attempt_date_time,rewards_deals_id,deal_id,created_date,updated_date)
VALUES (S.blocked_deals_id,S.user_id,S.blocked_till_date_time,S.unsuccessful_attempt_count,S.last_pin_attempt_date_time,S.rewards_deals_id,S.deal_id,S.created_date,S.updated_date)
