MERGE
  rms.rms_user_stamping_records T
USING
  (SELECT DISTINCT
	   stamps_record_id,
	    user_id as user_id,
	   stamp_card_id,
	   stamp_time,
	    geoX as geoX,
	    geoY as geoY,
	    ip_addr as ip_addr,
	   merchant_id,
	   from_state,
	   to_state,
	   merchant_store_id,
	   updated_date,
	    source as source,
	   success_status,
	    echoss_metadata as echoss_metadata
	FROM staging.rms_rmsdb_user_stamping_records) S
ON
T.stamps_record_id = S.stamps_record_id
WHEN MATCHED THEN UPDATE SET 
T.stamps_record_id = S.stamps_record_id,
T.user_id = S.user_id,
T.stamp_card_id = S.stamp_card_id,
T.stamp_time = S.stamp_time,
T.geox = S.geox,
T.geoy = S.geoy,
T.ip_addr = S.ip_addr,
T.merchant_id = S.merchant_id,
T.from_state = S.from_state,
T.to_state = S.to_state,
T.merchant_store_id = S.merchant_store_id,
T.updated_date = S.updated_date,
T.source = S.source,
T.success_status = S.success_status,
T.echoss_metadata = S.echoss_metadata
  WHEN NOT MATCHED
  THEN
INSERT
(stamps_record_id,user_id,stamp_card_id,stamp_time,geox,geoy,ip_addr,merchant_id,from_state,to_state,merchant_store_id,updated_date,source,success_status,echoss_metadata)
VALUES (S.stamps_record_id,S.user_id,S.stamp_card_id,S.stamp_time,S.geox,S.geoy,S.ip_addr,S.merchant_id,S.from_state,S.to_state,S.merchant_store_id,S.updated_date,S.source,S.success_status,S.echoss_metadata)
