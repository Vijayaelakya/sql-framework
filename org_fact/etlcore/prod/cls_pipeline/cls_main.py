import logging
from etl import cls

# from etlcore.prod.cls_pipeline.etl import cls

if __name__ == '__main__':
  logging.getLogger().setLevel(logging.INFO)
  cls.run_cls_pipeline()
