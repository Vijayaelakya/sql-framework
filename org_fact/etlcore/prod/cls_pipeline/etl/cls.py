#Import Libraries
import ast
import configparser
import cx_Oracle
import sys
from common.utility import get_connection_string_from_secretmanager, execute_query_and_get_as_parquet, \
    write_audit_to_bigquery, read_from_gcs
import mysql.connector
from mysql.connector import Error
import logging
import pyarrow as pa
import pyodbc
from enum import Enum


class Database(Enum):
    MYSQL = 'mysql'
    ORACLE = 'oracle'
    SQLSERVER = 'mssql'

connection = None
audit = {}
def run_cls_pipeline():
    try:
        args = dict([arg.split('=', maxsplit=1) for arg in sys.argv[1:]])

        secrets = configparser.ConfigParser()
        secrets.read_file(open('obfs_gcp_config.ini'))
        service_accnt_file = secrets.get('GCP', 'OBFS_GCP_SERVICE_ACCOUNT')
        secrets_conn = configparser.ConfigParser()
        secrets_conn.read_file(open('conn_details.ini'))

        # create oracle/mysql connection string from secret manager
        # connection_string = get_connection_string_from_secretmanager(args['project'],args['secret'],args['secret_version'])

        if args['module'] == 'cls':
            # connect_to_oracle
            connection_string = secrets_conn.get('DB_CONN', 'cls_conn_string')
            cx_Oracle.init_oracle_client(lib_dir=args['oracle_lib'])
            connection = cx_Oracle.connect(connection_string)
            # audit['source'] = 'oracle'
            audit['source'] = Database.ORACLE.value

        elif args['module'] == 'rms':
            # connect to mysql
            connection_string = secrets_conn.get('DB_CONN', 'rms_conn_string')
            x = connection_string.split(",")
            connection = mysql.connector.connect(host=x[0],user=x[1],password=x[2],database=x[3],ssl_disabled=True)
            # audit['source'] = 'mysql'
            audit['source'] = Database.MYSQL.value

        elif args['module'] == 'mob':
            # connect to mysql
            connection_string = secrets_conn.get('DB_CONN', 'mob_conn_string')
            x = connection_string.split(",")
            connection = mysql.connector.connect(host=x[0],user=x[1],password=x[2],database=x[3],ssl_disabled=True)
            # audit['source'] = 'mysql'
            audit['source'] = Database.MYSQL.value

        elif args['module'] == 'org_fact':
            # connect to MS SQL Server
            connection_string = secrets_conn.get('DB_CONN', 'org_factsql_conn_string')
            connection = pyodbc.connect(connection_string)
            audit['source'] = Database.SQLSERVER.value


        audit['dataset'] = args['dataset']
        audit['table'] = args['parquet_output_folder']
        audit['etlbatchid'] = args['etlbatchid']

        # read sql file from gcs
        sql_query = read_from_gcs(args['sql_query_bucket'],args['sql_query_file'], service_accnt_file)
        #print(sql_query)

        # read schema from gcs
        schema = read_from_gcs(args['sql_query_bucket'],args['schema_file'], service_accnt_file)
        #d = ast.literal_eval(schema.decode("utf-8"))
        d = ast.literal_eval(schema)

        fields = []
        if args['module'] == 'cls':
            for key, value in d.items():
                if value == 'integer':
                    fields.append(pa.field(key, pa.int64()))
                elif value == 'float':
                    fields.append(pa.field(key, pa.float64()))
                else:
                    fields.append(pa.field(key, pa.string()))
        elif args['module'] == 'rms' or args['module'] == 'mob':
            for key, value in d.items():
                if value == 'integer':
                    fields.append(pa.field(key, pa.int64()))
                elif value == 'float':
                    fields.append(pa.field(key, pa.float64()))
                elif value == 'date':
                    fields.append(pa.field(key,pa.timestamp('s',tz=None)))
                    #fields.append(pa.field(key, pa.date64()))
                    #fields.append(pa.field(key, pa.string()))
                else:
                    fields.append(pa.field(key, pa.string()))
        elif args['module'] == 'org_fact':
            for key, value in d.items():
                fields.append(pa.field(key, pa.string()))
            #    if value == 'integer':
            #        fields.append(pa.field(key, pa.int64()))
            #    elif value == 'float':
            #        fields.append(pa.field(key, pa.float64()))
            #    elif value == 'date':
            #        fields.append(pa.field(key,pa.timestamp('s',tz=None)))
            #    else:
            #        fields.append(pa.field(key, pa.string()))

        if args['parquet_output_folder'] == 'EFT_POS':
            fields.remove(pa.field('CONTACT_MOBILE', pa.int64()))
            fields.append(pa.field('CONTACT_MOBILE', pa.string()))

        pyarrow_schema = pa.schema(fields)
        #logging.info(pyarrow_schema)
        # fetch data from mysql/oracle/sqlserver and create folder with parquet files
        execute_query_and_get_as_parquet(connection, sql_query, args['parquet_output_folder'], args['fetch_limit'],
                                          pyarrow_schema, audit)

        # Write audit to BigQuery
        write_audit_to_bigquery(audit, args['dataset'], 'audit', service_accnt_file)

        # close connection
        # connection.close()

    except cx_Oracle.DatabaseError as exc:
        err, = exc.args
        logging.error("Oracle-Error-Code:", err.code)
        logging.error("Oracle-Error-Message:", err.message)
        # connection.close()
        raise
    except Error as e:
        err, = e.args
        logging.error("MySql-Error-Code:", err.code)
        logging.error("MySql-Error-Message:", err.message)
        # connection.close()
        raise
    except Exception as ex:
        logging.error("Error:  ")
        logging.error(ex)
        raise
    finally:
        connection.close()

