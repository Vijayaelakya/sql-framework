import cx_Oracle
from google.cloud import secretmanager, bigquery, storage
from pandas import DataFrame
import numpy as np
import pyarrow as pa
import pyarrow.parquet as pq
from mysql.connector import Error
import logging


def get_connection_string_from_secretmanager(project,secret,secret_version):
    # Get Cls/Rms/Mob AWS RDS credentials from Secret Manager
    try:
        client = secretmanager.SecretManagerServiceClient()
        name = client.secret_version_path(project, secret, secret_version)
        response = client.access_secret_version(name)
        return response.payload.data.decode('UTF-8')
    except Exception as e:
        logging.error("Could not fetch secret value for connection string:{} ",e)
        raise

def read_from_gcs(bucket,object_location, service_accnt_file):
    try:
        print(bucket)
        print(service_accnt_file)
        print(object_location)
        client = storage.Client.from_service_account_json(service_accnt_file)
        bucket = client.get_bucket(bucket)
        file_blob = bucket.get_blob(object_location)
        #print(file_blob.download_as_text(client=None))
        return file_blob.download_as_text(client=None)
    except Exception as e:
        logging.error("Error downloading SQL file from GCS:{}",e)
        raise

def write_audit_to_bigquery(audit_record, dataset, table, service_accnt_file):
    try:
        # Instantiates a client
        bigquery_client = bigquery.Client.from_service_account_json(service_accnt_file)

        # Prepares a reference to the dataset
        dataset_ref = bigquery_client.dataset(dataset)

        table_ref = dataset_ref.table(table)
        table = bigquery_client.get_table(table_ref)  # API call

        rows_to_insert = [
            audit_record
        ]
        bigquery_client.insert_rows(table, rows_to_insert)  # API request
    except Exception as e:
        logging.error("Failed while writing audit record to bigquery:{} ", e)
        raise

def execute_query_and_get_as_parquet(connection,query,parquet_file,fetch_limit, pyarrow_schema, audit):
    # try:
        # Execute Query
        cursor = connection.cursor()
        cursor.execute(query)
        count = 0
        columns = [desc[0] for desc in cursor.description]
        while True:
            records = cursor.fetchmany(int(fetch_limit))
            count = count + len(records)
            print(len(records))
            if not records:
                break
            df = DataFrame.from_records(records)
            
            print('\nPyArrow Schema: '+str(pyarrow_schema))
            df.columns = columns
            print(df)
            #df = df.astype(str)
            df = df.where(df.isnull(), df.astype(str))

            print(df)
            
            print(df.info(verbose=True))
            
            table = pa.Table.from_pandas(df, schema = pyarrow_schema)
            pq.write_to_dataset(table, root_path=parquet_file, compression='gzip')

        audit['records_count'] = count

        # Commit Changes and close cursor
        cursor.close()

