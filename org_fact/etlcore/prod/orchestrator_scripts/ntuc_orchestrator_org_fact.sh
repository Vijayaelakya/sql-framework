#!/bin/bash

cd /home/linketluser/org_fact/etlcore/prod/orchestrator_scripts/

export GOOGLE_APPLICATION_CREDENTIALS="obfs_gcp_service_account.json"

/usr/bin/python38 ntuc_orchestrator_org_fact.py > /home/linketluser/org_fact/etlcore/prod/orchestrator_scripts/logs/ntuc_orchestrator_org_fact_`date +\%Y\%m\%d`.log

