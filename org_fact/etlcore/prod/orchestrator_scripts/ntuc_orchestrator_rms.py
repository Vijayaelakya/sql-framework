import logging
import os
import sys
import subprocess
from datetime import date, datetime
import configparser
from google.cloud import bigquery
import shlex
from tenacity import *

#@retry(stop=stop_after_attempt(5),wait=wait_exponential(multiplier=1, min=4, max=10))
def subprocess_cmd(command):
  try:
    dt = datetime.now().strftime("%Y-%m-%d")
    staging_file ="ntuc_orchestrator_rms_" + dt + ".stdout"
    error_file = "ntuc_orchestrator_rms_" + dt + ".err"

    with open(error_file, "w+") as errfile:
        with open(staging_file, "w+") as staging:
            subprocess.run(shlex.split(command), stdout=staging, check=True, universal_newlines=True,timeout=3600,stderr=errfile)

  except Exception as e:
    raise e
  finally:
    print('===============STDOUT================')
    print(open('ntuc_orchestrator_rms_' + dt + '.stdout').read())
    print('===============STDERR================')
    print(open('ntuc_orchestrator_rms_' + dt + '.err').read())

def run():
  try:
    secrets = configparser.ConfigParser()
    secrets.read_file(open('link_gcp_config.ini'))
    service_accnt_file = secrets.get('GCP', 'LINK_GCP_SERVICE_ACCOUNT')

    subprocess_cmd("python38 /home/linketluser/linketlcore/urvashi/link-data-cloud/etlcore/prod/cls_pipeline_wrapper/cls_wrapper_main.py project=ne-link-data-cloud-production secret=mysql_conn_str secret_version=1 "
              "config_bucket=link-data-services-aws-migration table_config_file=cls/config/tables.json "
              "sha_config_file=cls/config/sha.json source=mysql module=rms sql_folder=rms/sql_files schema_folder=rms/schema_files "
              "conditional_table_config_file=cls/config/conditional_tables.json start_date=2020-10-08")
    sys.exit(0)

    rms = ['app_authentication', 'blocked_deals_rewards', 'blocked_stamps_rewards', 'collections_master',
           'deal_coupon_codes', 'deals_master', 'merchant_master', 'merchant_spoc', 'merchant_stores',
           'promotions_master', 'reward_promotion', 'rewards_deals', 'rewards_stamps', 'stamp_cards_master',
           'static_content_master', 'store_echoss_estamps', 'user_deal_records', 'user_stamping_records', 'user_table',
           'deals_collections', 'merchant_collections', 'promotions_collections', 'stamp_cards_collections']


    for table in rms:
        etlbatchid = datetime.now().strftime("%Y%m%d%H%M%S")

        extraction_job = "python38 /home/linketluser/linketlcore/urvashi/link-data-cloud/etlcore/prod/cls_pipeline/cls_main.py " \
                         "project=ne-link-data-cloud-production secret=mysql_conn_str secret_version=1 " \
                         "module=rms sql_query_bucket=link-data-services-aws-migration sql_query_file=rms/sql_files/{0}.sql " \
                         "schema_file=rms/schema_files/{0}.json parquet_output_folder={0} " \
                         "fetch_limit=100000 dataset=staging etlbatchid={1}".format(table,str(etlbatchid))

        # subprocess_cmd("cd ..; cd cls_pipeline; "+extraction_job)
        print("Extraction job for {0} started at = {1}".format(table,datetime.now().strftime("%H:%M:%S")))
        #subprocess_cmd("cd ..; cd cls_pipeline; rm -r "+table+"/; "+extraction_job)
        subprocess_cmd("rm -rf "+table+"/")
        subprocess_cmd(extraction_job)
        print("Extraction job for {0} ended at = {1}".format(table,datetime.now().strftime("%H:%M:%S")))

        today = date.today()
        subprocess_cmd("bq rm -f -t staging.rms_rmsdb_{0}".format(table))
        print("Copy/bq load  job for {0} started at = {1}".format(table, datetime.now().strftime("%H:%M:%S")))
        try:
            subprocess_cmd(
                "gsutil -m rm gs://link-data-services-aws-migration/rms/data_files/{0}/{1}/*".format(str(today),table)
                )
        except Exception as e:
            pass
        subprocess_cmd("gsutil -m cp -r {0} {1}"
                .format(table,
                    'gs://link-data-services-aws-migration/rms/data_files/' + str(today) + '/'))
        subprocess_cmd("bq load --replace --source_format=PARQUET staging.rms_rmsdb_{0} gs://link-data-services-aws-migration/rms/data_files/{1}/{2}/*"
                .format(table , str(today) , table))
        print("Copy/bq load job for {0} ended at = {1}".format(table, datetime.now().strftime("%H:%M:%S")))


        f = open("../merge_scripts/rms_merge/"+table+"_merge.sql", "r")
        print("Merge job for {0} started at = {1}".format(table, datetime.now().strftime("%H:%M:%S")))
        execute_redshift_bq_query(f.read(), service_accnt_file)
        print("Merge job for {0} ended at = {1}".format(table, datetime.now().strftime("%H:%M:%S")))

  except Exception as e:
    raise e

def execute_redshift_bq_query(query, service_accnt_file):
  try:
    
    client = bigquery.Client.from_service_account_json(service_accnt_file)
    query_job = client.query(query)
    results = query_job.result()  # Waits for job to complete.

  except Exception as e:
    print(e)
    raise e

if __name__ == '__main__':
  try:
    logging.getLogger().setLevel(logging.INFO)
    run()

  except Exception as e:
    print(e)
    raise e
