#!/bin/bash

cd /home/linketluser/linketlcore/urvashi/link-data-cloud/etlcore/prod/orchestrator_scripts/

date

/usr/bin/python38 ntuc_orchestrator_rms.py > /home/linketluser/linketlcore/urvashi/link-data-cloud/etlcore/prod/orchestrator_scripts/logs/ntuc_orchestrator_rms_`date +\%Y\%m\%d`.log

date
