import logging, sys
import os
import subprocess
from datetime import date, datetime
import configparser
from google.cloud import bigquery
import shlex
from tenacity import *

@retry(stop=stop_after_attempt(5),wait=wait_exponential(multiplier=1, min=4, max=10))
def subprocess_cmd(command):
  try:
    dt = datetime.now().strftime("%Y-%m-%d")
    staging_file ="ntuc_orchestrator_mob_" + dt + ".stdout"
    error_file = "ntuc_orchestrator_mob_" + dt + ".err"

    with open(error_file, "w+") as errfile:
        with open(staging_file, "w+") as staging:
            subprocess.run(shlex.split(command), stdout=staging, check=True, universal_newlines=True,timeout=3600,stderr=errfile)

  except Exception as e:
    raise e
  finally:
    print('===============STDOUT================')
    print(open('ntuc_orchestrator_mob_' + dt + '.stdout').read())
    print('===============STDERR================')
    print(open('ntuc_orchestrator_mob_' + dt + '.err').read())

def run():
  print("In run")
  try:
    secrets = configparser.ConfigParser()
    secrets.read_file(open('link_gcp_config.ini'))
    service_accnt_file = secrets.get('GCP', 'LINK_GCP_SERVICE_ACCOUNT')

    subprocess_cmd("python38 /home/linketluser/linketlcore/urvashi/link-data-cloud/etlcore/prod/cls_pipeline_wrapper/cls_wrapper_main.py project=ne-link-data-cloud-production secret=mob_conn_str secret_version=1 "
              "config_bucket=link-data-services-aws-migration table_config_file=cls/config/tables.json "
              "sha_config_file=cls/config/sha.json source=mysql module=mob sql_folder=mob/sql_files schema_folder=mob/schema_files "
              "conditional_table_config_file=cls/config/conditional_tables.json  start_date=2020-10-08")

    mob = ['address', 'address_audit', 'app_configure', 'app_configure_audit', 'comm_auth', 'comm_auth_audit',
           'email_otp', 'email_otp_audit', 'email_template', 'ntucp_postalcode', 'promo_code_transactions',
           'promo_codes', 'user_audit', 'user_auth', 'user_auth_audit', 'user_detail', 'user_meta', 'user_meta_audit','user_otp', 'user_otp_audit',
           'user_transaction_log', 'user_transaction_record']
    for table in mob:
        etlbatchid = datetime.now().strftime("%Y%m%d%H%M%S")

        extraction_job = "python38 /home/linketluser/linketlcore/urvashi/link-data-cloud/etlcore/prod/cls_pipeline/cls_main.py " \
                         "project=ne-link-data-cloud-production secret=mob_conn_str secret_version=1 " \
                         "module=mob sql_query_bucket=link-data-services-aws-migration sql_query_file=mob/sql_files/{0}.sql " \
                         "schema_file=mob/schema_files/{0}.json parquet_output_folder={0} " \
                         "fetch_limit=100000 dataset=staging etlbatchid={1}".format(table,str(etlbatchid))

        print("Extraction job for {0} started at = {1}".format(table, datetime.now().strftime("%H:%M:%S")))
        subprocess_cmd("rm -rf " + table +"/")
        subprocess_cmd(extraction_job)
        print("Extraction job for {0} ended at = {1}".format(table, datetime.now().strftime("%H:%M:%S")))
        
        today = date.today()
        subprocess_cmd("bq rm -f -t staging.mob_mobilityprod_{0}".format(table))
        print("Copy/bq load  job for {0} started at = {1}".format(table, datetime.now().strftime("%H:%M:%S")))
        try:
            subprocess_cmd(
                "gsutil -m rm gs://link-data-services-aws-migration/mob/data_files/{0}/{1}/*".format(str(today),table)
                )
        except Exception as e:
            pass
        subprocess_cmd("gsutil -m cp -r {0} {1}"
                .format(table,
                    'gs://link-data-services-aws-migration/mob/data_files/' + str(today) + '/'))
        subprocess_cmd("bq load --replace --source_format=PARQUET staging.mob_mobilityprod_{0} gs://link-data-services-aws-migration/mob/data_files/{1}/{2}/*"
                .format(table , str(today) , table))
        print("Copy/bq load job for {0} ended at = {1}".format(table, datetime.now().strftime("%H:%M:%S")))


        # subprocess_cmd("cd ../merge_scripts/mob_merge/;")
        f = open("../merge_scripts/mob_merge/" + table+"_merge.sql", "r")
        print("Merge job for {0} started at = {1}".format(table, datetime.now().strftime("%H:%M:%S")))
        execute_redshift_bq_query(f.read(), service_accnt_file)
        print("Merge job for {0} ended at = {1}".format(table, datetime.now().strftime("%H:%M:%S")))
  except Exception as e:
    raise e

def execute_redshift_bq_query(query, service_accnt_file):
  try:
    
    client = bigquery.Client.from_service_account_json(service_accnt_file)
    query_job = client.query(query)
    results = query_job.result()  # Waits for job to complete.

  except Exception as e:
    raise e


if __name__ == '__main__':
  try:
    logging.getLogger().setLevel(logging.INFO)
    run()

  except Exception as e:
    raise e

