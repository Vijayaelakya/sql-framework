#!/bin/bash

cd /home/linketluser/linketlcore/urvashi/link-data-cloud/etlcore/prod/orchestrator_scripts/

export LD_LIBRARY_PATH='/home/linketluser/linketlcore/instantclient_19_8'
today=$(date +"%Y-%m-%d")

/usr/bin/python38 ntuc_orchestrator.py > /home/linketluser/linketlcore/urvashi/link-data-cloud/etlcore/prod/orchestrator_scripts/logs/ntuc_orchestrator_${today}.log
