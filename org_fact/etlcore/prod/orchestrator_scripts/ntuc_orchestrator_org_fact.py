import logging
import os
import sys
import subprocess
from datetime import date, datetime
import configparser
from google.cloud import bigquery
import shlex
from tenacity import *
import sys
sys.path.append('/home/linketluser/org_fact/etlcore/prod/orchestrator_scripts')
import cron_google_chat_alert as gchat_alert


# @retry(stop=stop_after_attempt(5),wait=wait_exponential(multiplier=1, min=4, max=10))
def subprocess_cmd(command):
    try:
        dt = datetime.now().strftime("%Y-%m-%d")
        staging_file = "ntuc_orchestrator_org_fact_" + dt + ".stdout"
        error_file = "ntuc_orchestrator_org_fact_" + dt + ".err"

        with open(error_file, "w+") as errfile:
            with open(staging_file, "w+") as staging:
                subprocess.run(shlex.split(command), stdout=staging, check=True, universal_newlines=True, timeout=3600,
                               stderr=errfile)

    except Exception as e:
        raise e
    finally:
        print('===============STDOUT================')
        print(open('ntuc_orchestrator_org_fact_' + dt + '.stdout').read())
        print('===============STDERR================')
        print(open('ntuc_orchestrator_org_fact_' + dt + '.err').read())


def run():
    try:
        secrets = configparser.ConfigParser()
        secrets.read_file(open('obfs_gcp_config.ini'))
        service_accnt_file = secrets.get('GCP', 'OBFS_GCP_SERVICE_ACCOUNT')

        subprocess_cmd("python38 /home/linketluser/org_fact/etlcore/prod/cls_pipeline_wrapper/cls_wrapper_main.py " \
                       "project=ne-obfs-data-cloud-prod secret=mysql_conn_str secret_version=1 " \
                       "config_bucket=ne-obfs-data-cloud-prod-primary table_config_file=origins-fact/config/tables.json "
                       "sha_config_file=origins-fact/config/sha.json source=mssql module=org_fact sql_folder=origins-fact/sql_files schema_folder=origins-fact/schema_files "
                       "conditional_table_config_file=origins-fact/config/conditional_tables.json start_date=2020-10-08")

        # org_fact = ['dbo.BILL','dbo.BILLDET']
        org_fact = ['BILL', 'PRODUCT', 'PORDER', 'PORDDET', 'PRODMULT', 'MULTIUOM', 'CUSTOMER', 'SORDER',
                    'SORDDET','BILLDET', 'BILLIN', 'BLLINDET','SALERATE']

        for table in org_fact:
            etlbatchid = datetime.now().strftime("%Y%m%d%H%M%S")

            extraction_job = "python38 /home/linketluser/org_fact/etlcore/prod/cls_pipeline/cls_main.py " \
                             "project=ne-obfs-data-cloud-prod secret=mysql_conn_str secret_version=1 " \
                             "module=org_fact sql_query_bucket=ne-obfs-data-cloud-prod-primary sql_query_file=origins-fact/sql_files/{0}.sql " \
                             "schema_file=origins-fact/schema_files/{0}.json parquet_output_folder={0} " \
                             "fetch_limit=100000 dataset=data_staging etlbatchid={1}".format(table, str(etlbatchid))

            print("Extraction job for {0} started at = {1}".format(table, datetime.now().strftime("%H:%M:%S")))

            # subprocess_cmd("gcloud config set project ne-obfs-data-cloud-prod")

            # subprocess_cmd("gcloud auth activate-service-account data-cloud-admin@ne-obfs-data-cloud-prod.iam.gserviceaccount.com --key-file="+service_accnt_file+" --project=ne-obfs-data-cloud-prod")

            subprocess_cmd("rm -rf " + table + "/")

            subprocess_cmd(extraction_job)

            print("Extraction job for {0} ended at = {1}".format(table, datetime.now().strftime("%H:%M:%S")))

            today = date.today()
            subprocess_cmd("bq rm -f -t data_staging.org_fact_fa_origins_healthcare_{0}".format(table.lower()))
            print("Copy/bq load  job for {0} started at = {1}".format(table, datetime.now().strftime("%H:%M:%S")))
            try:
                subprocess_cmd(
                    "gsutil -m rm gs://ne-obfs-data-cloud-prod-primary/origins-fact/data_files/{0}/{1}/*".format(
                        str(today), table)
                )
            except Exception as e:
                pass
            subprocess_cmd("gsutil -m cp -r {0} {1}".format(table,
                                                            'gs://ne-obfs-data-cloud-prod-primary/origins-fact/data_files/' + str(
                                                                today) + '/'))
            # subprocess_cmd(
            #     "bq load --replace --source_format=PARQUET --project_id=ne-obfs-data-cloud-prod "
            #     "data_staging.org_fact_fa_origins_healthcare_{0} "
            #     "gs://ne-obfs-data-cloud-prod-primary/origins-fact/data_files/{1}/{2}/* "
            #     .format(table.lower(), str(today), table))
            load_to_staging(table,today)
            print("Copy/bq load job for {0} ended at = {1}".format(table, datetime.now().strftime("%H:%M:%S")))

            f = open("../merge_scripts/org_fact_merge/" + table + "_merge.sql", "r")
            print("Merge job for {0} started at = {1}".format(table, datetime.now().strftime("%H:%M:%S")))
            execute_redshift_bq_query(f.read(), service_accnt_file)
            print("Merge job for {0} ended at = {1}".format(table, datetime.now().strftime("%H:%M:%S")))

    except Exception as e:
        raise e


def execute_redshift_bq_query(query, service_accnt_file):
    try:

        client = bigquery.Client.from_service_account_json(service_accnt_file)
        query_job = client.query(query)
        results = query_job.result()  # Waits for job to complete.

    except Exception as e:
        print(e)
        raise e


def load_to_staging(table: str, today: date):
    try:
        client = bigquery.Client()
        staging_table = "ne-obfs-data-cloud-prod.data_staging.org_fact_fa_origins_healthcare_"+table.lower()
        #stg_table = "ne-obfs-data-cloud-prod.data_staging.unleashed_" + api_type
        # schema_file = api_type + "_schema.json"
        # table = client.get_table(stg_table)
        # with open(schema_file, "w+") as f:
        #     client.schema_to_json(table.schema, f)
        # schema = client.schema_from_json(api_type + '_schema.json')

        job_config = bigquery.LoadJobConfig(
            write_disposition=bigquery.WriteDisposition.WRITE_TRUNCATE,
            create_disposition=bigquery.CreateDisposition.CREATE_IF_NEEDED,
            ignore_unknown_values=True,
            source_format=bigquery.SourceFormat.PARQUET,
        )
        uri = "gs://ne-obfs-data-cloud-prod-primary/origins-fact/data_files/"+str(today)+"/"+table+"/*"

        load_job = client.load_table_from_uri(
            uri,
            staging_table,
            job_config=job_config,
        )
        load_job.result()
        destination_table = client.get_table(staging_table)
        print("\nStaging table loaded with {} rows.".format(destination_table.num_rows))

    except Exception as expt:
        print(expt)
        raise


if __name__ == '__main__':
    try:
        logging.getLogger().setLevel(logging.INFO)
        run()

    except Exception as e:
        gchat_alert.main("*Org_fact ETL job failed* : " + table + " table")
        raise e

