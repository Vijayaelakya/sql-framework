from json import dumps
from httplib2 import Http
import sys

url="https://chat.googleapis.com/v1/spaces/AAAA0ErDDHc/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=t1uEoj2znAAmYpvwOHdB2WQVnf6_lE058Or-xhFNa0c%3D"

def main(message):
    try:
        """Hangouts Chat incoming webhook quickstart."""

        bot_message = {'text' : message}

        message_headers = {'Content-Type': 'application/json; charset=UTF-8'}

        http_obj = Http()

        response = http_obj.request(
                uri=url,
                method='POST',
                headers=message_headers,
                body=dumps(bot_message),
                )
        print(response)

    except Exception as e:
        print(e)
        raise e

if __name__ == '__main__':
    try:
        main(sys.argv[1])
    except Exception as e:
        print(e)
        raise e
