import logging
import os
import subprocess
from datetime import datetime
import datetime as dtm
from datetime import date as d
from google.cloud import bigquery
import configparser
import shlex
from pytz import reference
from tenacity import *

@retry(stop=stop_after_attempt(5),wait=wait_exponential(multiplier=1, min=4, max=10))
def subprocess_cmd(command):
  try:
    dt = datetime.now().strftime("%Y-%m-%d")
    staging_file ="ntuc_orchestrator_cls_transaction_" + dt + ".stdout"
    error_file = "ntuc_orchestrator_cls_transaction_" + dt + ".err"

    with open(error_file, "w+") as errfile:
        with open(staging_file, "w+") as staging:
            subprocess.run(shlex.split(command), stdout=staging, check=True, universal_newlines=True,timeout=3600,stderr=errfile)

  except Exception as e:
    raise e
  finally:
    print('===============STDOUT================')
    print(open('ntuc_orchestrator_cls_transaction_' + dt + '.stdout').read())
    print('===============STDERR================')
    print(open('ntuc_orchestrator_cls_transaction_' + dt + '.err').read())



def run():
    try:
        os.chdir('/home/linketluser/linketlcore/urvashi/link-data-cloud/etlcore/prod/orchestrator_scripts')
        localtime = reference.LocalTimezone()
        now = datetime.now()
        secrets = configparser.ConfigParser()
        secrets.read_file(open('link_gcp_config.ini'))
        service_accnt_file = secrets.get('GCP', 'LINK_GCP_SERVICE_ACCOUNT')
        
        print("Process started at = ",  now.strftime("%m-%d-%Y, %H:%M:%S," + localtime.tzname(now)))
        week_ago = d.today() - dtm.timedelta(days=7)
        subprocess_cmd(
        "python38 /home/linketluser/linketlcore/urvashi/link-data-cloud/etlcore/prod/cls_pipeline_wrapper/cls_wrapper_main.py project=ne-link-data-cloud-production secret=conn_str secret_version=1 "
        "config_bucket=link-data-services-aws-migration table_config_file=cls/config/tables.json "
        "sha_config_file=cls/config/sha.json source=oracle module=cls "
        "oracle_lib=/home/linketluser/linketlcore/instantclient_19_8/ sql_folder=cls/sql_files "
        "schema_folder=cls/schema_files conditional_table_config_file=cls/config/conditional_tables.json start_date=" + str(week_ago))
        
        print("Schema/query files ready at = ",  now.strftime("%m-%d-%Y, %H:%M:%S," + localtime.tzname(now)))
        transactions = ['award_transaction_history', 'redemption_transaction', 'adjustment_transaction']
        
        for table in transactions:
            etlbatchid = datetime.now().strftime("%Y%m%d%H%M%S")
            
            extraction_job = "python38 /home/linketluser/linketlcore/urvashi/link-data-cloud/etlcore/prod/cls_pipeline/cls_main.py project=ne-link-data-cloud-production secret=conn_str secret_version=1 " \
                         "module=cls oracle_lib=/home/linketluser/linketlcore/instantclient_19_8/ " \
                         "sql_query_bucket=link-data-services-aws-migration sql_query_file=cls/sql_files/{0}.sql " \
                         "schema_file=cls/schema_files/{0}.json parquet_output_folder=/home/linketluser/linketlcore/urvashi/link-data-cloud/etlcore/prod/cls_pipeline/{0} " \
                         "fetch_limit=100000 dataset=staging etlbatchid={1}".format(table.upper(), str(etlbatchid))
                         
            print("Extraction job for {0} started at = {1}".format(table,  now.strftime("%m-%d-%Y, %H:%M:%S," + localtime.tzname(now))))
            subprocess_cmd("rm -rf /home/linketluser/linketlcore/urvashi/link-data-cloud/etlcore/prod/cls_pipeline/"+table.upper()+"/")
            subprocess_cmd(extraction_job)
            
            print("Extraction job for {0} ended at = {1}".format(table,  now.strftime("%m-%d-%Y, %H:%M:%S," + localtime.tzname(now))))
            today = d.today()
            subprocess_cmd("bq rm -f -t staging.cls_blp_{0}".format(table))
            print("Copy/bq load  job for {0} started at = {1}".format(table,  now.strftime("%m-%d-%Y, %H:%M:%S," + localtime.tzname(now))))
            try:
                subprocess_cmd("gsutil -m rm gs://link-data-services-aws-migration/cls/data_files/{0}/{1}/*".format(str(today),
                                                                                                            table.upper()))
            except Exception as e:
                pass
            
            subprocess_cmd("gsutil -m cp -r {0} {1}"
                       .format("/home/linketluser/linketlcore/urvashi/link-data-cloud/etlcore/prod/cls_pipeline/" + table.upper(),'gs://link-data-services-aws-migration/cls/data_files/' + str(today) + '/'))
            subprocess_cmd("bq load --source_format=PARQUET staging.cls_blp_{3} gs://link-data-services-aws-migration/cls/data_files/{4}/{1}/*"
                       .format("/home/linketluser/linketlcore/urvashi/link-data-cloud/etlcore/prod/cls_pipeline/" + table.upper(),table.upper(),
                    'gs://link-data-services-aws-migration/cls/data_files/' + str(today) + '/', table, str(today)))
            print("Copy/bq load job for {0} ended at = {1}".format(table,  now.strftime("%m-%d-%Y, %H:%M:%S," + localtime.tzname(now))))
            
            merge_query_files = ['txn_all_merge_1.sql', 'txn_all_merge_2.sql', 'txn_all_merge_3.sql']
            
            for merge_query in merge_query_files:
                f = open("../merge_scripts/cls_merge/" + merge_query, "r")
                print("Merge job for {0} started at = {1}".format(table,  now.strftime("%m-%d-%Y, %H:%M:%S," + localtime.tzname(now))))
                execute_redshift_bq_query(f.read(), service_accnt_file)
                print("Merge job for {0} ended at = {1}".format(table,  now.strftime("%m-%d-%Y, %H:%M:%S," + localtime.tzname(now))))

    except Exception as exp:
        print(exp)
        raise

def execute_redshift_bq_query(query, service_accnt_file):
    try:
        client = bigquery.Client.from_service_account_json(service_accnt_file)
        query_job = client.query(query)
        results = query_job.result()  # Waits for job to complete.
    except Exception as exp:
        print(exp)
        raise

if __name__ == '__main__':
    try:
        logging.getLogger().setLevel(logging.INFO)
        run()
    except Exception as exp:
        print(exp)
        raise

