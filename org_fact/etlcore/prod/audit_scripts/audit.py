#!/usr/bin/python3

from google.cloud import bigquery
from google.oauth2 import service_account
from datetime import date
import pandas_gbq

def read_from_bq(project, link_dataset, table_list, prod_dataset):
    credentials = service_account.Credentials.from_service_account_file(
    "/home/linketluser/linketlcore/urvashi/link-data-cloud/etlcore/prod/audit_scripts/link_gcp_service_account.json", scopes=["https://www.googleapis.com/auth/cloud-platform"],)
    # client = bigquery.Client(credentials=credentials, project=credentials.project_id)
    query = "SELECT t1.table_id,TIMESTAMP_MILLIS(t1.last_modified_time) last_modified_time, " \
            "t1.row_count as link_mob_row_count, t2.row_count as mob_row_count, " \
            "CURRENT_DATE('Asia/Singapore') run_time FROM `_link_dataset_.__TABLES__` t1 inner join " \
            "(select table_id,row_count from `_prod_dataset_.__TABLES__`) t2 " \
            "on t1.table_id = t2.table_id where t1.table_id in (_table_names_)"
    query = query.replace('_link_dataset_', link_dataset).replace('_prod_dataset_',prod_dataset).replace('_table_names_', table_list)
    print(query)
    df = pandas_gbq.read_gbq(query, project_id=project, credentials=credentials)
    pandas_gbq.to_gbq(df, link_dataset+'.audit_table', project_id=project, if_exists='append')



if __name__ == '__main__':
    list_of_tables = {
             "link_mob" : "'mob_address', 'mob_address_audit', 'mob_app_configure', 'mob_app_configure_audit', 'mob_comm_auth', 'mob_comm_auth_audit', 'mob_email_otp', 'mob_email_otp_audit', 'mob_email_template', 'mob_ntucp_postalcode', 'mob_promo_code_transactions', 'mob_promo_codes', 'mob_user_audit', 'mob_user_auth', 'mob_user_auth_audit', 'mob_user_detail', 'mob_user_meta', 'mob_user_meta_audit', 'mob_user_otp', 'mob_user_otp_audit', 'mob_user_transaction_log', 'mob_user_transaction_record'",
             "link_gcp": "'link_cls_attributes', 'link_cls_branch', 'link_cls_card', 'link_cls_card_bin', 'link_cls_client', 'link_cls_corporation', 'link_cls_eft_pos_group','link_cls_establishment', 'link_cls_establishment_group', 'link_cls_loyalty_account_group', 'link_cls_loyalty_account_product', 'link_cls_pool_definition', 'link_cls_product_account', 'link_cls_product_account_type', 'link_cls_product_code', 'link_cls_proxy_card_mapping', 'link_cls_scheme', 'link_cls_scheme_counter', 'link_cls_scheme_criteria', 'link_cls_scheme_rule_master', 'link_cls_transaction_code', 'link_cls_loyalty_account', 'link_cls_loyalty_account_extension', 'link_cls_terminal_scheme_pool', 'link_cls_loyalty_account_balance', 'link_cls_txn_all'",
              "link_rms" : "'rms_app_authentication', 'rms_blocked_deals_rewards', 'rms_blocked_stamps_rewards', 'rms_collections_master', 'rms_deal_coupon_codes', 'rms_deals_master', 'rms_merchant_master', 'rms_merchant_spoc', 'rms_merchant_stores', 'rms_promotions_master', 'rms_reward_promotion', 'rms_rewards_deals', 'rms_rewards_stamps', 'rms_stamp_cards_master', 'rms_static_content_master', 'rms_store_echoss_estamps', 'rms_user_deal_records', 'rms_user_stamping_records', 'rms_user_table', 'rms_deals_collections', 'rms_merchant_collections', 'rms_promotions_collections', 'rms_stamp_cards_collections'",
              "edw_gcp" : "'dim_campaign_sfmc', 'dim_branch', 'dim_card', 'dim_date', 'dim_journey_sfmc', 'dim_merchant', 'dim_scheme', 'fact_campaign_performance_360', 'fact_cls_transactions', 'fact_sfmc_comms_metrics', 'fact_sfmc_conversion', 'fact_journey_comms_activity', 'fact_journey_non_comms_activity'"}
    prod_dataset = {
                    "link_mob" : "mob",
                    "link_gcp":"link",
                    "link_rms": "rms",
                    "edw_gcp" : "edw"
                    }
    for link_dataset in list_of_tables.keys():
        read_from_bq('ne-link-data-cloud-production', link_dataset, list_of_tables[link_dataset], prod_dataset[link_dataset])
