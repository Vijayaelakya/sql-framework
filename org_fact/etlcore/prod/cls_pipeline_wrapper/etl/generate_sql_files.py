import json
import logging,sys
#from mysql.connector import Error
from google.cloud import storage
from pandas import DataFrame

# Import statements for each "source" db type are moved within each function due to address underlying ODBC module errors

def generate_mssqlserver_sql(connection_string, database, table_schema, tables, config_bucket, sql_folder, schema_folder,
                       sha_dict, conditional_tables_dict, start_date, service_accnt_file):
    
    # Imports are moved under function to avoid ODBC collision between pyodbc & mysql.connector
    import pyodbc
    
    try:

        cnxn= pyodbc.connect(connection_string)
        cursor = cnxn.cursor()
        cursor.execute("select table_name, column_name, column_default, is_nullable, data_type, numeric_precision, "
                       "numeric_scale from INFORMATION_SCHEMA.COLUMNS WHERE table_name in "+tables)
        
        # Get columns
        columns = [i[0] for i in cursor.description]
        print(columns)

        # Fetch all records and convert to dataframe
        records = cursor.fetchall()
        df = DataFrame.from_records(records)
        df.columns = columns
        #print(df)

        # convert dataframe to json string
        j = (df.groupby(['table_name'], as_index=True)
             .apply(lambda x: x[['column_name', 'column_default', 'is_nullable', 'data_type', 'numeric_precision',
                                 'numeric_scale']].to_dict('records'))
             .reset_index()
             .rename(columns={0: 'SCHEMA_INFO'})
             .to_json(orient='records'))

        all_table_info = json.loads(j)
        for table_info in all_table_info:
            schema_info = table_info['SCHEMA_INFO']
            create_schema = {}
            mssqlserver_extraction_query = ''

            for x in schema_info:
                if (x['data_type']).lower() in {'datetime','date'}:
                    #mssqlserver_extraction_query = mssqlserver_extraction_query + ' CASE WHEN CONVERT(DATE, [' + x['column_name'] + ']) = \'1900-01-01\' THEN NULL ELSE '+ x['column_name']+' END as ' + x['column_name'] + ','
                    mssqlserver_extraction_query = mssqlserver_extraction_query + 'ISNULL(FORMAT([' + x['column_name'] + '],\'yyyy-MM-dd HH:mm:ss\'),\'1900-01-01 00:00:00\') AS ' + x['column_name'] + ', '
                    create_schema[x['column_name']] = 'date'

                elif (x['data_type']).lower() in {'decimal','double','float','numeric'}:
                    mssqlserver_extraction_query = mssqlserver_extraction_query + 'ISNULL([' + x['column_name'] + '],0.0) AS ' + x['column_name'] + ', '
                    create_schema[x['column_name']] = 'float'

                elif (x['data_type']).lower() in {'int','bigint','tinyint','smallint','bit'}:
                    mssqlserver_extraction_query = mssqlserver_extraction_query + 'ISNULL([' + x['column_name'] + '],0) AS ' + x['column_name'] + ', '
                    create_schema[x['column_name']] = 'integer'
                elif (x['data_type']).lower() in {'image'}:
                    mssqlserver_extraction_query = mssqlserver_extraction_query + ' NULL AS '+ x['column_name'] + ', '
                else:
                    #mssqlserver_extraction_query = mssqlserver_extraction_query + '[' + x['column_name'] + '], '
                    mssqlserver_extraction_query = mssqlserver_extraction_query + 'CASE WHEN LEN(RTRIM(LTRIM(CAST (' + x['column_name'] + ' AS VARCHAR))))<1 THEN NULL ELSE '+ x['column_name'] +' END AS ' + x['column_name'] + ' , ' 
                    create_schema[x['column_name']] = 'string'
            mssqlserver_extraction_query = "select  " + mssqlserver_extraction_query + 'from "' + database + '".' + table_schema + '.' + table_info['table_name']

            last_char_index = mssqlserver_extraction_query.rfind(',')
            final_query_string = mssqlserver_extraction_query[:last_char_index] + "" + mssqlserver_extraction_query[last_char_index + 1:]

            #print(final_query_string)

            # logging.info(final_query_string)
            # sys.exit(0)
            # Write to query file
            storage_client = storage.Client.from_service_account_json(service_accnt_file)
            bucket = storage_client.get_bucket(config_bucket)
            sql_file = bucket.blob(sql_folder + '/' + table_info['table_name'] + '.sql')
            sql_file.upload_from_string(final_query_string)

            # Write to schema file
            sql_file = bucket.blob(schema_folder + '/' + table_info['table_name'] + '.json')
            sql_file.upload_from_string(str(create_schema))


    except Exception as error:
        print(error)
        raise


def generate_oracle_sql(oracle_lib, connection_string, owner, tables, config_bucket, sql_folder, schema_folder,
                        sha_dict, conditional_tables_dict, start_date, service_accnt_file):

    # Conditional Import  
    import cx_Oracle
    
    # create oracle connection
    cx_Oracle.init_oracle_client(lib_dir=oracle_lib)
    connection = cx_Oracle.connect(connection_string)

    try:
            cursor = connection.cursor()
            cursor.execute("select table_name, column_name, data_type, data_length, data_precision, data_scale, nullable, "
                   "data_default from ALL_TAB_COLUMNS where owner = '"+owner.upper()+"' and table_name in " + tables)

            # Get columns
            columns = [i[0] for i in cursor.description]

            # Fetch all records and convert to dataframe
            records = cursor.fetchall()
            df = DataFrame(records)
            df.columns = columns

            # convert dataframe to json string
            j = (df.groupby(['TABLE_NAME'], as_index=True)
                 .apply(lambda x: x[['COLUMN_NAME', 'DATA_TYPE', 'DATA_LENGTH', 'DATA_PRECISION', 'DATA_SCALE', 'NULLABLE',
                                     'DATA_DEFAULT']].to_dict('r'))
                 .reset_index()
                 .rename(columns={0: 'SCHEMA_INFO'})
                 .to_json(orient='records'))

            all_table_info = json.loads(j)
            for table_info in all_table_info:
                schema_info = table_info['SCHEMA_INFO']
                create_schema = {}
                oracle_extraction_query = 'select '

                for x in schema_info:
                    if x['DATA_TYPE'] == 'DATE' or x['DATA_TYPE'] == 'TIMESTAMP(0)' \
                            or x['DATA_TYPE'] == 'TIMESTAMP(6)' or x['DATA_TYPE'] == 'TIMESTAMP(9)':
                        oracle_extraction_query = oracle_extraction_query \
                                + 'to_char(' + x['COLUMN_NAME'] + ', \'yyyy-mm-dd hh24:mi:ss\') as ' + x['COLUMN_NAME'] + ', '
                                # + 'concat(to_char(to_date(' + x['COLUMN_NAME'] + ', \'dd-mm-RRRR\'), \'yyyy-mm-dd\'),' \
                                # 'to_char(' + x['COLUMN_NAME'] + ', \' hh24:mi:ss\')) as ' + x['COLUMN_NAME'] + ', '
                                # Add :ff6 for microseconds

                        create_schema[x['COLUMN_NAME']] = 'date'

                    elif x['DATA_TYPE'] == 'NUMBER' and x['DATA_SCALE'] != None and int(x['DATA_SCALE']) > 0:
                        oracle_extraction_query = oracle_extraction_query + 'ROUND(' + x['COLUMN_NAME'] + ', 5) as ' + x[
                            'COLUMN_NAME'] + ', '
                        create_schema[x['COLUMN_NAME']] = 'float'

                    elif x['DATA_TYPE'] == 'NUMBER' and (x['DATA_SCALE'] == None or int(x['DATA_SCALE']) == 0):
                        oracle_extraction_query = oracle_extraction_query + x['COLUMN_NAME'] + ', '
                        create_schema[x['COLUMN_NAME']] = 'integer'

                    else:
                        oracle_extraction_query = oracle_extraction_query + x['COLUMN_NAME'] + ', '
                        create_schema[x['COLUMN_NAME']] = 'string'

                oracle_extraction_query = oracle_extraction_query + 'from ' + owner + '.' + table_info[
                    'TABLE_NAME']
                last_char_index = oracle_extraction_query.rfind(',')
                final_query_string = oracle_extraction_query[:last_char_index] + "" + oracle_extraction_query[
                                                                                      last_char_index + 1:]

                # for txn tables only
                if table_info['TABLE_NAME'] in conditional_tables_dict:
                    conditional_columns = str(conditional_tables_dict[table_info['TABLE_NAME']]).split(",")
                    count = 0
                    for column in conditional_columns:
                        if count == 0:
                            final_query_string = final_query_string + ' where ' + column + ' >= TO_DATE(\'' + start_date + '\', \'YYYY-MM-DD\')'
                        else:
                            final_query_string = final_query_string + ' or ' + column + ' >= TO_DATE(\'' + start_date + '\', \'YYYY-MM-DD\')'

                # encrypt SHA columns
                if table_info['TABLE_NAME'] in sha_dict:
                    sha_columns = str(sha_dict[table_info['TABLE_NAME']]).split(",")
                    for column in sha_columns:
                        final_query_string = final_query_string.replace(column,'cast(standard_hash('+column+',\'SHA256\') as varchar2(1000)) as '+column)

                # Write to query file
                storage_client = storage.Client.from_service_account_json(service_accnt_file)
                bucket = storage_client.get_bucket(config_bucket)
                sql_file = bucket.blob(sql_folder + '/' + table_info['TABLE_NAME'] + '.sql')
                sql_file.upload_from_string(final_query_string)

                # Write to schema file
                sql_file = bucket.blob(schema_folder + '/' + table_info['TABLE_NAME'] + '.json')
                sql_file.upload_from_string(str(create_schema))

    except ValueError as error:
        logging.error(error.args)
        raise


def generate_mysql_sql(connection_string, table_schema, tables, config_bucket, sql_folder, schema_folder,
                       sha_dict, conditional_tables_dict, start_date, service_accnt_file):
    
    # Conditional Import
    import mysql.connector
    from mysql.connector import Error
    
    x = connection_string.split(",")
    connection = mysql.connector.connect(host=x[0], user=x[1], password=x[2], ssl_disabled=True)
    try:
        cursor = connection.cursor()
        cursor.execute("select table_name, column_name, column_default, is_nullable, data_type, numeric_precision, "
                       "numeric_scale from INFORMATION_SCHEMA.COLUMNS WHERE table_name in "+tables)

        # Get columns
        columns = [i[0] for i in cursor.description]

        # Fetch all records and convert to dataframe
        records = cursor.fetchall()
        df = DataFrame(records)
        df.columns = columns

        # convert dataframe to json string
        j = (df.groupby(['table_name'], as_index=True)
             .apply(lambda x: x[['column_name', 'column_default', 'is_nullable', 'data_type', 'numeric_precision',
                                 'numeric_scale']].to_dict('r'))
             .reset_index()
             .rename(columns={0: 'SCHEMA_INFO'})
             .to_json(orient='records'))

        all_table_info = json.loads(j)
        for table_info in all_table_info:
            schema_info = table_info['SCHEMA_INFO']
            create_schema = {}
            mysql_extraction_query = ''

            for x in schema_info:
                if (x['data_type']).lower() == 'datetime':
                    mysql_extraction_query = mysql_extraction_query + x['column_name'] + ', '
                    create_schema[x['column_name']] = 'date'

                elif (x['data_type']).lower() == 'double' or (x['data_type']).lower() == 'float':
                    mysql_extraction_query = mysql_extraction_query + x['column_name'] + ', '
                    create_schema[x['column_name']] = 'float'

                elif (x['data_type']).lower() == 'int' or (x['data_type']).lower() == 'tinyint':
                    mysql_extraction_query = mysql_extraction_query + x['column_name'] + ', '
                    create_schema[x['column_name']] = 'integer'

                else:
                    mysql_extraction_query = mysql_extraction_query + x['column_name'] + ', '
                    create_schema[x['column_name']] = 'string'
            # encrypt SHA columns
            if (table_info['table_name']).upper() in sha_dict:
                logging.info("inside if")
                sha_columns = str(sha_dict[(table_info['table_name']).upper()]).split(",")
                table_columns = mysql_extraction_query.split(",")
                for i in range(len(table_columns)):
                    for column in sha_columns:
                        # logging.info(table_columns[i])
                        # logging.info(column)
                        if table_columns[i].strip().lower() == column.lower():
                            # logging.info("Found match")
                            table_columns[i] = 'SHA2(' + column.lower() + ',256) as ' + column.lower()
                mysql_extraction_query = ", ".join(cols for cols in table_columns)

            mysql_extraction_query = "select " + mysql_extraction_query + 'from ' + table_schema + '.' + table_info[
                'table_name']
            #logging.info(mysql_extraction_query)

            # for txn tables only
            if table_info['table_name'] in conditional_tables_dict:
                conditional_columns = str(conditional_tables_dict[table_info['table_name']]).split(",")
                count = 0
                for column in conditional_columns:
                    if count == 0:
                        mysql_extraction_query = mysql_extraction_query + ' where ' + column + ' >= ' + start_date
                    else:
                        mysql_extraction_query = mysql_extraction_query + ' or ' + column + ' >= ' + start_date

            last_char_index = mysql_extraction_query.rfind(',')
            final_query_string = mysql_extraction_query[:last_char_index] + "" + mysql_extraction_query[
                                                                                  last_char_index + 1:]

            # logging.info(final_query_string)
            # sys.exit(0)
            # Write to query file
            storage_client = storage.Client.from_service_account_json(service_accnt_file)
            bucket = storage_client.get_bucket(config_bucket)
            sql_file = bucket.blob(sql_folder + '/' + table_info['table_name'] + '.sql')
            sql_file.upload_from_string(final_query_string)

            # Write to schema file
            sql_file = bucket.blob(schema_folder + '/' + table_info['table_name'] + '.json')
            sql_file.upload_from_string(str(create_schema))

    except ValueError as error:
        logging.error(error.args)
        raise
