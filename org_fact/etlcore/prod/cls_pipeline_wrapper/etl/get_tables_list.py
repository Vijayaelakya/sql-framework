import json
import logging
from google.cloud import storage


def get_list_of_tables(config_bucket, config_file, module, service_accnt_file):
    # Get the list of tables and owner for a module
    try:
        storage_client = storage.Client.from_service_account_json(service_accnt_file)
        bucket = storage_client.get_bucket(config_bucket)
        blob = bucket.blob(config_file)
        if None != blob:
            json_tables_list = json.loads(blob.download_as_string(client=None))
            try:
                return json_tables_list[module]
            except:
                raise ValueError('Module "'+module+'" does not exist in the file.')
        else:
            raise ValueError('Json config file does not exist in provided GCS location. Please check.')
    except ValueError as error:
        logging.error(error.args)
        raise

def get_dict_of_sha_columns(config_bucket, config_file, module, service_accnt_file):
    # Get the list of tables and owner for a module
    try:
        storage_client = storage.Client.from_service_account_json(service_accnt_file)
        bucket = storage_client.get_bucket(config_bucket)
        blob = bucket.blob(config_file)
        if None != blob:
            json_sha_tables_list = json.loads(blob.download_as_string(client=None))
            try:
                json_sha_list = json_sha_tables_list[module]
                sha = {}
                for x in json_sha_list:
                    arr = x.upper().split(":")
                    sha[arr[0]] = arr[1]
                return sha
            except:
                raise ValueError('Module "'+module+'" does not exist in the file.')
        else:
            raise ValueError('Json config file does not exist in provided GCS location. Please check.')
    except ValueError as error:
        logging.error(error.args)
        raise