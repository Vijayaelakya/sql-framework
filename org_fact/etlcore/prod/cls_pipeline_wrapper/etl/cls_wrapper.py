import configparser
import sys
# from common.utility import get_connection_string_from_secretmanager
#from etlcore.prod.cls_pipeline_wrapper.etl.generate_sql_files import generate_oracle_sql, generate_mysql_sql
#from etlcore.prod.cls_pipeline_wrapper.etl.get_tables_list import get_list_of_tables, get_dict_of_sha_columns
from etl.generate_sql_files import generate_oracle_sql, generate_mysql_sql,generate_mssqlserver_sql
from etl.get_tables_list import get_list_of_tables, get_dict_of_sha_columns


def run():
    args = dict([arg.split('=', maxsplit=1) for arg in sys.argv[1:]])

    secrets = configparser.ConfigParser()
    secrets.read_file(open('obfs_gcp_config.ini'))
    service_accnt_file = secrets.get('GCP', 'OBFS_GCP_SERVICE_ACCOUNT')
    secrets_conn = configparser.ConfigParser()
    secrets_conn.read_file(open('conn_details.ini'))
    print(list(secrets_conn['DB_CONN'].keys()))

    # Get the list of tables and owner for a module
    tables_list = []
    try:
        tables_list = get_list_of_tables(args['config_bucket'],args['table_config_file'],args['module'], service_accnt_file)
        sha_dict = get_dict_of_sha_columns(args['config_bucket'], args['sha_config_file'], args['module'], service_accnt_file)
        conditional_tables_dict = get_dict_of_sha_columns(args['config_bucket'], args['conditional_table_config_file'],
                                                          args['module'], service_accnt_file)

    except ValueError as error:
        raise

    if None != tables_list:
        tables = [x.upper().split('.')[1] for x in tables_list]
        owner = tables_list[0].upper().split('.')[0]

        # Get oracle connection string
        # payload = get_connection_string_from_secretmanager(args['project'],args['secret'],args['secret_version'])

        if args['module'] == 'cls':
            payload = secrets_conn.get('DB_CONN', 'cls_conn_string')
            # Query all_tab_columns and generate .sql files
            generate_oracle_sql(args['oracle_lib'], payload, owner, str(tuple(tables)), args['config_bucket'],
                               args['sql_folder'], args['schema_folder'], sha_dict, conditional_tables_dict, args['start_date'], service_accnt_file)
            print("CLS SQL Download Completed.")

        elif args['module'] == 'rms':
            # Query all_tab_columns and generate .sql files
            payload = secrets_conn.get('DB_CONN', 'rms_conn_string')
            generate_mysql_sql(payload, owner.lower(), str(tuple(tables)), args['config_bucket'],
                               args['sql_folder'], args['schema_folder'], sha_dict, conditional_tables_dict, args['start_date'], service_accnt_file)
            print("RMS SQL Download Completed.")

        elif args['module'] == 'mob':
            # Query all_tab_columns and generate .sql files
            payload = secrets_conn.get('DB_CONN', 'mob_conn_string')
            generate_mysql_sql(payload, owner.lower(), str(tuple(tables)), args['config_bucket'],
                               args['sql_folder'], args['schema_folder'], sha_dict, conditional_tables_dict, args['start_date'], service_accnt_file)

            print("MOB SQL Download Completed.")
        
        elif args['module'] == 'org_fact':
            # Query all_tab_columns and generate .sql files
            print(args['source'])
            payload = secrets_conn.get('DB_CONN','org_factsql_conn_string')
            database = secrets_conn.get('DB_CONN','ORG_FACT_DB_NAME')
            generate_mssqlserver_sql(payload,database,owner.upper(),str(tuple(tables)), args['config_bucket'],
                                args['sql_folder'], args['schema_folder'], sha_dict, conditional_tables_dict, args['start_date'], service_accnt_file)
            
            print("ORG FACT SQL Download Completed.")
