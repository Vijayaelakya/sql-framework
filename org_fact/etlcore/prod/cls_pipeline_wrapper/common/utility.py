import logging
from google.cloud import secretmanager


def get_connection_string_from_secretmanager(project,secret,secret_version):
    # Create the Secret Manager client.
    try:
        client = secretmanager.SecretManagerServiceClient()
        name = client.secret_version_path(project, secret, secret_version)
        response = client.access_secret_version(name)
        return response.payload.data.decode('UTF-8')
    except Exception as e:
        logging.error("Could not fetch secret value for connection string:{} ",e)
        raise
