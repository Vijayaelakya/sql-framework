import logging

from etl import cls_wrapper

if __name__ == '__main__':
    try:
        logging.getLogger().setLevel(logging.INFO)
        cls_wrapper.run()
    except Exception as e:
        raise e
